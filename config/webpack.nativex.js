const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/wrapper/NativeX.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist/nativex'),
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({  // Also generate a test.html
          filename: 'inline_source.html',
          inject: 'body',
          inlineSource: '.(js|css)$',
          templateContent: `
            <!DOCTYPE html>
            <html>
            
            <head>
                <meta charset="utf-8">
                <title>Game Template</title>
                <style>
                    ::-webkit-input-placeholder { /* Edge */
                        color: orange;
                    }
            
                    :-ms-input-placeholder { /* Internet Explorer 10-11 */
                        color: orange;
                    }
            
                    ::placeholder {
                        color: orange;
                    }
                </style>
            </head>
            
            <body>
            </body>
            
            </html>`,
        }),
        new HtmlWebpackInlineSourcePlugin()
    ]
};