const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/wrapper/Ironsource.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist/ironsource'),
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({  // Also generate a test.html
          filename: 'inline_source.html',
          inject: 'body',
          inlineSource: '.(js|css)$',
          templateContent: `
            <!DOCTYPE html>
            <html>
            
            <head>
                <meta charset="utf-8">
                <title>Game Template</title>
                <style>
                    ::-webkit-input-placeholder { /* Edge */
                        color: orange;
                    }
            
                    :-ms-input-placeholder { /* Internet Explorer 10-11 */
                        color: orange;
                    }
            
                    ::placeholder {
                        color: orange;
                    }
                </style>
                <script>function getScript(e,n){var i=document.createElement("script");i.type="text/javascript",i.async=!0,n&&(i.onload=n),i.src=e,document.head.appendChild(i)}function parseMessage(e){var i=e.data;return n="string"==typeof i?i.indexOf(DOLLAR_PREFIX+RECEIVE_MSG_PREFIX):-1,-1!==n?getMessageParams(i.slice(n+2)):{}}function getMessageParams(e){var n,i=[],t=e.split("/"),a=t.length;if(-1===e.indexOf(RECEIVE_MSG_PREFIX)){if(a>=2&&a%2==0)for(n=0;a>n;n+=2)i[t[n]]=t.length<n+1?null:decodeURIComponent(t[n+1])}else{var o=e.split(RECEIVE_MSG_PREFIX);void 0!==o[1]&&(i=JSON&&JSON.parse(o[1]))}return i}function getDapi(e){var n=parseMessage(e);n&&n.name!==GET_DAPI_URL_MSG_NAME||getScript(n.data,onDapiReceived)}function invokeDapiListeners(){for(var e in dapiEventsPool)dapiEventsPool.hasOwnProperty(e)&&dapi.addEventListener(e,dapiEventsPool[e])}function onDapiReceived(){dapi=window.dapi,window.removeEventListener("message",getDapi),invokeDapiListeners()}function init(){window.dapi.isDemoDapi&&(window.parent.postMessage(DOLLAR_PREFIX+SEND_MSG_PREFIX+JSON.stringify({state:"getDapiUrl"}),"*"),window.addEventListener("message",getDapi,!1))}var DOLLAR_PREFIX="$$",RECEIVE_MSG_PREFIX="DAPI_SERVICE:",SEND_MSG_PREFIX="DAPI_AD:",GET_DAPI_URL_MSG_NAME="connection.getDapiUrl",dapiEventsPool={},dapi=window.dapi||{isReady:function(){return!1},addEventListener:function(e,n){dapiEventsPool[e]=n},removeEventListener:function(e){delete dapiEventsPool[e]},isDemoDapi:!0};init();</script>
            </head>
            
            <body>
            </body>
            
            </html>`,
        }),
        new HtmlWebpackInlineSourcePlugin()
    ]
};