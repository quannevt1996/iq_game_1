// const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const networks = require('./networks');
//CONFIG DIR
const confDir = './config/';
//EDIT THIS FOR BASE PROD MODE CONFIG
const baseConfig = {
    ...common,
    mode: 'production',
    devtool: 'source-map',
}
// EDIT THIS FOR SEPERATED CONFIG
const modedConfigs = {
    default: require(confDir + 'webpack.default'),
    ironsource: require(confDir + 'webpack.ironsource'),
    nativex: require(confDir + 'webpack.nativex'),
    mraid: require(confDir + 'webpack.mraid'),
    uac: require(confDir + 'webpack.uac'),
};
//
const configs = () => {
    let arr = [];
    for (let a in networks)
    {
        if (networks[a])
        {
            arr.push({
                ...baseConfig,
                ...modedConfigs[`${a}`],
            });
        }
    }
    return arr;
}
//
module.exports = configs;