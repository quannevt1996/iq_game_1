import _ from 'lodash';
import './style.css';
// require.context('./assets/', true, /\.(png|svg|jpg|gif|jfif)$/);
const MidiParser = require('./libs/MidiParser');
//
if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    // console.warn("Production mode, console cleared");
    // console.log = function(){};
}

//
global.PIXI = require('pixi.js');
global.Utils = require('./core/Utils');
global.EventDefine = require('./events/EventDefine');
global.Override = require('./core/Override');
global.Input = require('./core/Input');
//

// <!-- The core Firebase JS SDK is always required and must be listed first -->
// <script src="/__/firebase/8.2.9/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->

// <!-- Initialize Firebase -->
// <script src="/__/firebase/init.js"></script>

const styles = {
    canvasStyle : {
        // position: 'absolute'
    },
    containerStyle : {
        // textAlign: 'center'
    },
    gameDivStyle : {
        // position: 'relative'
        alignItems: 'center',
        justifyContent: 'center',
        /* align-self: center; */
        height: '100vh',
        display: 'flex',
    }
}

class Index
{
    constructor()
    {
        this.Resize;
        this.VisibleChange;
        this.AudioChange;
        this.singleCall;
    }

    CreateElement()
    {
        const div = document.createElement('div');
        const element = document.createElement('div');
        element.appendChild(div);
        // var mdSource = "data:audio/midi;base64,TVRoZAAAAAYAAQACAGBNVHJrAAAAEwD/UQMHCuIA/1gEBAIYCAD/LwBNVHJrAAACaQD/AwlGTCBLZXlzIDEAkDJeAZBHUQCQOUwBkEI5FpBJVSyARwAekFE4VoBJAAuQSVVZgDkAAYBCAAmAMgBYgFEAAIBJAAKQNEwCkDtZAZBEOmCQRzUNgEQAC5BJWhuQSkkPgEkAD5BJZgmASgAzgEcAHpBFPAiASQALkFFKIoBFAA6QUE8BkEdOCYBRABqQRTcHgDsAA4A0AAGARwAAkE5EAJA2PQGQPUsEkEdQC4BQABqARQA4kEVPIIBHAGGARQANgD0AAJA9RSeATgAJkEIcCJBFMyiQR0AwkElCMJBMWAOAQgAFgEcAAIBJACiQTk9gkEJHA5BJYguAPQAAgEwABYBFACGQVVIBgE4AJYBJAAGQRT0KgEIACZBTSgWQR2QKkE5iKYBVAA+AUwACgEUABYA2AACARwADgE4AAZAyXgGQR1EAkDlMAZBCORaQSVUsgEcAHpBROFaASQALkElVWYA5AAGAQgAJgDIAWIBRAACASQACkDRMApA7WQGQRDpgkEc1DYBEAAuQSVobkEpJD4BJAA+QSWYJgEoAM4BHAB6QRTwIgEkAC5BRSiKARQAOkFBPAZBHTgmAUQAakEU3B4A7AAOANAABgEcAAJBORACQNj0BkD1LBJBHUAuAUAAagEUAOJBFTyCARwBhgEUADYA9AACQPUUngE4ACZBCHAiQRTMokEdAMIA2AACAPQAAgEIAAIBFAACARwAAkE5EAJA2PQGQPUsEkEdQXZBFTyCARwBhgEUADYA9AACQPUUngE4ACZBCHAiQRTMokEpAMIA2AACAPQAAgEIAAIBFAACASgAA/y8A";
        // // Create midi parser
        // var parser = new MidiParser();
        // var parsedMidi = parser.parseDataUrl(mdSource);
        // console.log(parsedMidi);

        // Lodash, currently included via a script, is required for this line to work
        // element.innerHTML = _.join(['Hello', 'webpack'], ' ');
        div.id = 'canvasContainer';
        for (let a in styles.gameDivStyle) div.style[a] = styles.gameDivStyle[a];
        for (let a in styles.containerStyle) element.style[a] = styles.containerStyle[a];
        //
        document.body.appendChild(element);
        document.body.style.overflow = "hidden";
    }

    MainGameClosure()
    {
        // (function() {

            var APP = require('./app');
            var StateManager = require('./states/StateManager');
        
            var states = [
                new (require('./states/StatePreload')),
                new (require('./states/StateLoading')),
                new (require('./states/StateIngame')),
                new (require('./states/StateEnd'))
            ];
            for (let a in styles.canvasStyle) APP.view.style[a] = styles.canvasStyle[a];
            document.getElementById('canvasContainer').appendChild(APP.view);
            //
            StateManager.AddState(states);
            //
            APP.Init();
            // APP.Resize();
            StateManager.Init();
            global.Resources = PIXI.Loader.shared.resources;
            //
            APP.renderer.plugins.interaction.moveWhenInside = true;
            // console.log(APP);
        
            // global.resize = function(size)
            // {
            //     APP.Resize(size);
            // }
            this.Resize = function(size) {
                APP.Resize(size);
            }

            this.VisibleChange = function(){

            }

            this.AudioChange = function() {

            }
        
            // global.resize({ width: innerWidth, height: innerHeight });
            
        // }.bind(this)());
    }

    OnReady()
    {
        if (!this.singleCall)
        {
            this.singleCall = true;
            this.CreateElement();
            this.MainGameClosure();
        }
    }
}

export default Index;

// function component()
// {
//     const div = document.createElement('div');
//     const element = document.createElement('div');
//     element.appendChild(div);
//     // var mdSource = "data:audio/midi;base64,TVRoZAAAAAYAAQACAGBNVHJrAAAAEwD/UQMHCuIA/1gEBAIYCAD/LwBNVHJrAAACaQD/AwlGTCBLZXlzIDEAkDJeAZBHUQCQOUwBkEI5FpBJVSyARwAekFE4VoBJAAuQSVVZgDkAAYBCAAmAMgBYgFEAAIBJAAKQNEwCkDtZAZBEOmCQRzUNgEQAC5BJWhuQSkkPgEkAD5BJZgmASgAzgEcAHpBFPAiASQALkFFKIoBFAA6QUE8BkEdOCYBRABqQRTcHgDsAA4A0AAGARwAAkE5EAJA2PQGQPUsEkEdQC4BQABqARQA4kEVPIIBHAGGARQANgD0AAJA9RSeATgAJkEIcCJBFMyiQR0AwkElCMJBMWAOAQgAFgEcAAIBJACiQTk9gkEJHA5BJYguAPQAAgEwABYBFACGQVVIBgE4AJYBJAAGQRT0KgEIACZBTSgWQR2QKkE5iKYBVAA+AUwACgEUABYA2AACARwADgE4AAZAyXgGQR1EAkDlMAZBCORaQSVUsgEcAHpBROFaASQALkElVWYA5AAGAQgAJgDIAWIBRAACASQACkDRMApA7WQGQRDpgkEc1DYBEAAuQSVobkEpJD4BJAA+QSWYJgEoAM4BHAB6QRTwIgEkAC5BRSiKARQAOkFBPAZBHTgmAUQAakEU3B4A7AAOANAABgEcAAJBORACQNj0BkD1LBJBHUAuAUAAagEUAOJBFTyCARwBhgEUADYA9AACQPUUngE4ACZBCHAiQRTMokEdAMIA2AACAPQAAgEIAAIBFAACARwAAkE5EAJA2PQGQPUsEkEdQXZBFTyCARwBhgEUADYA9AACQPUUngE4ACZBCHAiQRTMokEpAMIA2AACAPQAAgEIAAIBFAACASgAA/y8A";
//     // // Create midi parser
//     // var parser = new MidiParser();
//     // var parsedMidi = parser.parseDataUrl(mdSource);

//     // Lodash, currently included via a script, is required for this line to work
//     // element.innerHTML = _.join(['Hello', 'webpack'], ' ');
//     div.id = 'canvasContainer';
//     for (let a in styles.gameDivStyle) div.style[a] = styles.gameDivStyle[a];
//     for (let a in styles.containerStyle) element.style[a] = styles.containerStyle[a];

//     return element;
// }
// //
// document.body.appendChild(component());
//
// (function() {

//     var APP = require('./app');
//     var StateManager = require('./states/StateManager');

//     var states = [
//         new (require('./states/StatePreload')),
//         new (require('./states/StateLoading')),
//         new (require('./states/StateIngame'))
//     ];
//     for (let a in styles.canvasStyle) APP.view.style[a] = styles.canvasStyle[a];
//     document.getElementById('canvasContainer').appendChild(APP.view);
//     //
//     StateManager.AddState(states);
//     //
//     APP.Init();
//     StateManager.Init();
//     global.Resources = PIXI.Loader.shared.resources;
//     //
//     APP.renderer.plugins.interaction.moveWhenInside = true;
//     // console.log(APP);

//     global.resize = function(size)
//     {
//         APP.Resize(size);
//     }

//     global.resize({ width: innerWidth, height: innerHeight });
    
// }());

// window.addEventListener('resize', (event) => {
//     const el = document.getElementById('canvasContainer');
//     global.resize({width: el.clientWidth, height: el.clientHeight});
// }, false);