const EventDefine = require('./events/EventDefine');
const EventManager = require('./events/EventManager');
const GameDefine = require('./game/GameDefine');

const settings = {
    antialias: false,
    autoDensity: false, // !!!
};

class Application extends PIXI.Application
{
    constructor(settings)
    {
        super(settings);
        this.width = 768;
        this.height = 1366;
        this.scale = 1;
        this.renderer.resize(this.width * this.scale, this.height * this.scale);
        this.renderer.backgroundColor = 0xffffff;
        this.maxRatio = 1.5;
    }

    Init()
    {
        this.ticker.add(() => {
            let deltaTime = this.ticker.elapsedMS/1000;
            // this.stage.scale.set(this.scale);
        });
    }

    IsWideView()
    {
        return this.width/this.height > this.maxRatio;
    }

    GetWidth()
    {
        return this.width * this.scale;
    }

    GetHeight()
    {
        return this.height * this.scale;
    }

    GetGameView()
    {
        let ratio = this.width/this.height;
        return {
            width: this.IsWideView() ? this.width * this.maxRatio/ratio : this.width,
            height: this.height,
        };
    }

    GetOffset()
    {
        let ratio = this.width/this.height;
        return {
            x: this.IsWideView() ? (this.width*this.scale - (this.width*this.maxRatio/ratio))/2 : 0,
            y: 0,
        };
    }

    Resize(size)
    {
        if (!size)
        {
            // size = {
            //     width: this.view.parentElement.clientWidth, 
            //     height: this.view.parentElement.clientHeight
            // };
            // size = {
            //     width: window.outerWidth,
            //     height: window.outerHeight
            // };
        }
        // console.log(size);
        let baseRatio = this.width/this.height;
        let ratio = size.width/size.height;
        if (ratio > baseRatio)
        {
            //landscape
            this.view.style.width = 'auto';
            this.view.style.height = '100%';
        }
        else
        {
            //portrait
            this.view.style.width = '100%';
            this.view.style.height = 'auto';
            // console.log('here 2', size.width);
        }
        if (size.width >= size.height)
        {
            this.width = size.width;
            this.height = size.height;
            // this.height = 768;
        }
        else
        {
            // if (size.width < 768)
            // {
            //     let r = 768/size.width;
            //     let r2 = 1366/size.height;
            //     let r3 = ratio/baseRatio;
            //     this.width = 768;
            //     this.height = 1366;
            // }
            // else
            // {
            //     this.width = size.width;
            //     this.height = size.height;
            // }
            // this.width = 768;
            // this.height = 1366;
            this.width = size.width;
            this.height = size.height;
        }
        if (this.IsWideView()) 
            this.scale = (this.width/this.height) / this.maxRatio;
        else this.scale = 1;
        this.renderer.resize(this.width * this.scale, this.height * this.scale);
        // console.log(this);
        GameDefine.Resize(this);
        EventManager.publish(EventDefine.ON_CONTAINER_RESIZE, {data: size});
    }
}

module.exports = new Application(settings);