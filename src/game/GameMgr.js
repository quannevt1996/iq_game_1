const ScoreMgr = require('./ScoreMgr');
const Timer = require('../core/Timer');
const RankBar = require('./RankBar');
const ComboMgr = require('./ComboMgr');
const APP = require('../app');
const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');
const ParticleMgr = require('./ParticleMgr');
const GameDefine = require('./GameDefine');
const Levels = require('./Levels/index');

class GameMgr extends PIXI.Container
{
    constructor()
    {
        super();
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mState;
        this.mIsFirstInit = false;
        this.mTimer = new Timer();
        this.mTimer.SetDuration(30);
        //
        this.mScoreMgr = null;
        this.mRankBar = null;
        this.mComboMgr = null;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                // this.mTimer.Update(dt);
                // if (this.mTimer.IsDone())
                // {
                //     let event = new CustomEvent("gameEnd", {  });
                //     window.dispatchEvent(event);
                // }
                this.mRankBar.Update(dt);
                // this.mComboMgr.Update(dt);
                this.mLevels.Update(dt);
                ParticleMgr.Update(dt);
            break;
        }
    }

    //PRIVATE FUNCTION
    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                if (!this.mIsFirstInit)
                {
                    this.mIsFirstInit = true;
                    this.mScoreMgr = ScoreMgr;
                    this.mRankBar = new RankBar();
                    // this.mComboMgr = new ComboMgr();
                    this.mLevels = new Levels();
                    //
                    this.mScoreMgr.Init();
                    this.mRankBar.Init();
                    // this.mComboMgr.Init();
                    this.mLevels.Init();
                    //
                    this.mRankBar.position.set(0.5*APP.GetWidth(), 20);
                    // this.mComboMgr.position.set(0.5*APP.GetWidth(), 200);
                    //
                    // this.addChild(this.mRankBar);
                    // this.addChild(this.mComboMgr);
                    this.addChild(this.mLevels);
                    this.addChild(ParticleMgr.GetParticleContainer(GameDefine.CONFETTI_PARTICLE));
                }
                this.Start();
            break;

            case this.STATE.RUN:
                ParticleMgr.Start();
            break;
        }
    }
}

module.exports = GameMgr;