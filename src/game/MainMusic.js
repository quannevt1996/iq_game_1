const {Howl} = require('howler');
const Timer = require('../core/Timer');
const GameDefine = require('../game/GameDefine');

class MainMusic extends Howl
{
    constructor(props)
    {
        super(props);
        //
        this.mPauseTime = new Timer();
        this.mPauseTime.SetDuration(props.pauseDuration ? props.pauseDuration : 0);
        this.mPlayDelay = false;
    }

    Update(dt)
    {
        if (this.mPlayDelay)
        {
            this.mPauseTime.Update(dt);
            if (this.mPauseTime.IsDone())
            {
                this.mPlayDelay = false;
                this.play();
            }
        }
    }

    GetCurrentTime()
    {
        return this.seek();
    }

    play()
    {
        if (this.mPauseTime.IsDone())
            super.play();
        else
            this.mPlayDelay = true;
    }

    pause()
    {
        super.pause();
        this.mPlayDelay = false;
    }

    stop()
    {
        super.stop();
        this.mPlayDelay = false;
    }
}

let m = new MainMusic({
    // pauseDuration: GameDefine.GAME_TIME_PAUSE_BEFORE_PLAY,
    // src: [Utils.GetAssetUrl(GameDefine.BACKGROUND_MUSIC_PATH)]
    // src: [Utils.GetAssetUrl("sounds/TheBlackEyedPeas_ads.mp3")]
});

module.exports = m;