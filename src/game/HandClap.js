class HandClap extends PIXI.Container
{
    constructor(opts = {leftHand: null, righHand: null})
    {
        super();
        this.mLeftHand = new PIXI.Sprite(opts.leftHand);
        this.mRightHand = new PIXI.Sprite(opts.rightHand);
        this.mLeftHand.anchor.set(0.5, 1);
        this.mRightHand.anchor.set(0.5, 1);
        this.addChild(this.mRightHand, this.mLeftHand);
        this.mDeltatime1 = 3*Math.PI/2;
        this.mDeltatime2 = Math.PI/2;
        this.mLeftHand.x += 10;
        this.mRightHand.x -= 10;
    }

    Update(dt)
    {
        this.mLeftHand.rotation = Math.PI/48*Math.sin(this.mDeltatime1) + Math.PI/48;
        this.mRightHand.rotation = Math.PI/48*Math.sin(this.mDeltatime2) - Math.PI/48;
        this.mDeltatime1 = Math.min(7/2*Math.PI, this.mDeltatime1 + 40*dt);
        this.mDeltatime2 = Math.min(5/2*Math.PI, this.mDeltatime2 + 40*dt);
        if (this.mDeltatime1 == 7/2*Math.PI)
            this.mDeltatime1 = 3*Math.PI/2;
        if (this.mDeltatime2 == 5/2*Math.PI)
            this.mDeltatime2 = Math.PI/2;
    }
}

module.exports = HandClap;