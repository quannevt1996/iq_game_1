const APP = require('../app');
const GameDefine = require('./GameDefine');
// const NoteInfor = require('../assets/sounds/DanceMonkeyCover_TonesAndI.json');
const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');

class RankBar extends PIXI.Container
{
    constructor()
    {
        let n = 0;
        super();
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mRankSprites = [];
        this.mFillMask = null;
        this.mFill = null;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.INIT:
            break;

            case this.STATE.RUN:
                this._UpdateFillAlpha(dt);
            break;
        }
    }

    InitUI()
    {
        let barLength = GameDefine.RANK_BAR_LENGTH/100 * APP.GetWidth();
        let barBackground = new PIXI.Graphics();
        this.mFill = new PIXI.Graphics();
        let totalRank = GameDefine.TOTAL_RANK;
        let barPosY = 5;
        //
        barBackground.beginFill(0xffffff, 0.7);
        barBackground.drawRect(-0.5*barLength, 0, barLength, 10);
        barBackground.endFill();
        this.mFill.beginFill(0xfcc603, 1);
        this.mFill.drawRect(-0.5*barLength, 0, barLength, 10);
        this.mFill.endFill();
        this.mFill.deltaAlpha = 0;
        // barBackground.position.set(0.5*APP.GetWidth(), barPosY);
        //
        for (let i = 0; i< totalRank; i++)
        {
            if (i < 3)
            {
                let starSprite = new PIXI.Sprite(Resources.spritesheet.textures['star.png']);
                starSprite.anchor.set(0.5);
                starSprite.position.set(-0.5*barLength + (barLength/totalRank)*(i+1), barPosY);
                starSprite.scale.set(1.5);
                // starSprite.deltaAlpha = 0;
                this.mRankSprites.push(starSprite);
                continue;
            }
            else
            {
                let crownSprite = new PIXI.Sprite(Resources.spritesheet.textures['crown.png']);
                crownSprite.anchor.set(0.5);
                crownSprite.position.set(-0.5*barLength + (barLength/totalRank)*(i+1), barPosY -5);
                crownSprite.scale.set(1.5);
                // crownSprite.deltaAlpha = 0;
                this.mRankSprites.push(crownSprite);
            }
        }
        //
        this.mFillMask = new PIXI.Graphics();
        this.mFillMask.beginFill(0xffffff, 1);
        this.mFillMask.drawRect(0, 0, barLength, 10);
        this.mFillMask.endFill();
        this.mFillMask.position.set(-0.5*barLength, 0);
        this.mFillMask.scale.set(0, 1);
        //
        this.mFill.mask = this.mFillMask;
        //
        this.addChild(barBackground);
        this.addChild(this.mFill);
        this.addChild(this.mFillMask);
        this.addChild(...this.mRankSprites);
    }

    //PRIVATE FUNCTION

    _UpdateFillAlpha(dt)
    {
        // this.mFill.alpha = 0.25 * Math.cos(this.mFill.deltaAlpha+=10*dt) + 0.75;
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                this.InitUI();
                this.Start();
                EventManager.subscribe(EventDefine.ON_SCORE_SUCCESS, function(){
                    let scale = this.mFillMask.scale.x;
                    let curRank = scale*GameDefine.TOTAL_RANK;
                    this.mFillMask.scale.set(scale + 1/(GameDefine.NOTE_DATA.length - 1), 1);
                    // console.log(scale, GameDefine.TOTAL_RANK);
                    // for (let i = 0; i< Math.floor(curRank); i++)
                    for (let i = 0; i< Math.floor(Math.round((curRank + Number.EPSILON) * 100) / 100); i++)
                    {
                        if (curRank == 0)
                            break;
                        else
                        {
                            if (this.mRankSprites[i].tint != GameDefine.RANK_TINT_COLOR)
                                this.mRankSprites[i].tint = GameDefine.RANK_TINT_COLOR;
                        }
                    }
                    // console.log(scale);
                }.bind(this));
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = RankBar;