const GameDefine = require('../GameDefine');
const APP = require('../../app');
const Character = require('../Character');
const Input = require('../../core/Input');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Utils = require('../../core/Utils');
const Timer = require('../../core/Timer');
const { Howl } = require('howler');
const ParticleMgr = require('../ParticleMgr');

//CHAR DEFINE
const CHAR1_ANCHOR_POSITION          = [{x: 0, y: 0}, {x: 0, y: 0}, {x: 0.74, y: 0.24}];
const CHAR1_BODY_RIG_POSITION        = [{x: 0, y: 0}, {x: 19, y: 256}];
const CHAR2_ANCHOR_POSITION          = [{x: 0, y: 0}, {x: 0, y: 0}, {x: 0.82, y: 0.1}];
const CHAR2_BODY_RIG_POSITION        = [{x: 0, y: 0}, {x: 153, y: 158}];
// const CHECK_MARK_POS                 = {left: {x: 0, y: 0}, mid: {x: 100, y: 100}, right: {x: 250, y: -50}};

class LevelSurvive extends PIXI.Container
{
    constructor()
    {
        super();
        this.mIsFirstInit = false;
        this.mBackground = new PIXI.Container();
        this.char1 = null;
        this.char2 = null;
        this.mTapPos = {x: null, y: null};
        // this.checkMark1 = new PIXI.Graphics();
        // this.checkMark2 = new PIXI.Graphics();
        this.curCheckMarkPos = {x: 0, y: 0};
        this.mCongratTimer = new Timer();
        this.container = new PIXI.Container();
        this.tree = null;
        this.cloud1 = null;
        this.cloud2 = null;
        this.isTap = false;
        this.optA = null;
        this.optB = null;
        this.dark = null;
        this.isLast = false;
        this.checkMark = null;
        this.backgroundCheck = null;
        this.soundCheck = null;
        this.tickSound = new Howl({
            src: [Utils.GetAssetUrl("sounds/marksound.mp3")],
            autoplay: false,
            loop: false
        });
        this.hand = null;
        this.delayTut = new Timer();
        this.arrowTut = null;
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: 'black',
                fontSize: '80px',
                lineJoin: "bevel",
                stroke: "black",
                strokeThickness: 1,
                fontWeight: 'bold'
            });
            const lineStyle  = {
                native: false,
                width: 30,
                color: 0x11b18f,
                alignment: 0.5,
                alpha: 1,
                join: 'round',
                lineCap: 'round',
                miterLimit: 198
            }
            //
            this.mIsFirstInit = true;
            //
            let t = new PIXI.Sprite(Resources.surviveMask.texture)
            // let b = new PIXI.Sprite(Resources.bgBottom.texture);
            let m = new PIXI.Graphics();
            this.mBackground = new PIXI.Sprite(Resources.surviveMask.texture);
            this.mBackground.anchor.set(0.5, 1);
            m.beginFill(0xd3eef5, 1);
            m.drawRect(-1*APP.GetWidth(), -1*APP.GetHeight(), 2*APP.GetWidth(), 2*APP.GetHeight());
            //
            this.title = new PIXI.Text('Who will survive ?', style);
            this.title.anchor.set(0.5);
            this.tree = new PIXI.Sprite(Resources.surviveTree.texture);
            this.tree.anchor.set(0.5);
            this.cloud1 = new PIXI.Sprite(Resources.surviveCloud.texture);
            this.cloud2 = new PIXI.Sprite(Resources.surviveCloud.texture);
            this.optA = new PIXI.Sprite(Resources.gameplay_optA.texture);
            this.optB = new PIXI.Sprite(Resources.gameplay_optB.texture);
            this.optA.anchor.set(0.5);
            this.optB.anchor.set(0.5);
            this.dark = new PIXI.Graphics();
            this.dark.beginFill(0x000000, 0.4);
            this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
            this.dark.alpha = 0;
            this.checkMark = new PIXI.Sprite(Resources.correctTick.texture);
            let c = new PIXI.Graphics();
            c.beginFill(0x000000, 1);
            c.drawRect(0, 0, this.checkMark.width, this.checkMark.height);
            c.scale.x = 0;
            // this.checkMark.addChild(c);
            this.checkMark.mask = c;
            this.backgroundCheck = new PIXI.Graphics();
            this.backgroundCheck.beginFill(0x34eb74, 0.8);
            this.backgroundCheck.drawCircle(0, 0, this.checkMark.width);
            this.backgroundCheck.visible = false;
            //
            this.hand = new PIXI.Sprite(Resources.tutHand.texture);
            this.hand.anchor.set(0.5);
            this.arrowTut = new PIXI.Sprite(Resources.arrowTut.texture);
            this.arrowTut.anchor.set(0.5);
            this.arrowTut.alpha = 0;
            //
            this.delayTut.SetDuration(1);
            //
            // this.checkMark1.lineStyle(lineStyle);
            // this.checkMark1.roundPixels = true;
            // this.checkMark2.lineStyle(lineStyle);
            this.mCongratTimer.SetDuration(3.5);
            this.SetTouchable(true);
            this.InitCharacter();
            //
            this.container.mask = this.mBackground;
            this.container.addChild(m);
            this.container.addChild(this.cloud1, this.cloud2);
            this.container.addChild(this.tree);
            this.container.addChild(this.char1, this.char2);
            this.addChild(this.mBackground);
            // this.addChild(t);
            this.addChild(this.container);
            this.addChild(this.backgroundCheck);
            this.addChild(this.optA, this.optB);
            this.addChild(this.checkMark);
            this.addChild(c);
            this.addChild(this.arrowTut);
            // this.addChild(this.checkMark1, this.checkMark2);
            this.Scaling();
            this.Positioning();
            this.EventHandler();
        }
    }

    Resize()
    {
        this.Scaling();
        this.Positioning();
    }

    AddUIParent()
    {
        this.parent.UIContainer.addChild(this.title);
        this.parent.UIContainer.addChild(this.dark);
        this.parent.UIContainer.addChild(this.hand);
    }


    InitCharacter()
    {
        this.char1 = new Character({
            head: null,
            body: Resources.survive_char1Body.texture,
            legs: Resources.survive_char1Legs.texture,
            anchors: CHAR1_ANCHOR_POSITION,
            positions: CHAR1_BODY_RIG_POSITION
        });
        this.char1.isDrop = false;
        this.char1.deltaTime = 0;
        this.char1.pivot.x = this.char1.width - 170;
        this.char1.pivot.y = 0;
        // console.log(this.char1.width);
        // this.char1.rotation = Math.PI/4;
        //
        this.char2 = new Character({
            head: null,
            body: Resources.survive_char2Body.texture,
            legs: Resources.survive_char2Legs.texture,
            anchors: CHAR2_ANCHOR_POSITION,
            positions: CHAR2_BODY_RIG_POSITION
        });
        this.char2.isDrop = false;
        this.char2.deltaTime = 0;
        this.char2.pivot.x = 10;
        this.char2.pivot.y = 74;
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.ON_TOUCH, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_MOVE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_RELEASE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.STATE_INGAME_COMPLETE, function(){
            if (this.isLast)
                this.dark.visible = false;
        }.bind(this));
    }

    Positioning()
    {
        // this.mBackground.position.set(0.5 * APP.GetWidth() - 0.5*this.mBackground.width, 0.5 * APP.GetHeight() - 0.5*this.mBackground.height);
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mBackground.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 200);
            this.title.position.set(0.5*APP.GetWidth(), 150);
            this.char1.position.set(20, -240);
            this.char2.position.set(120, -210);
            this.cloud1.position.set(-260, -240);
            this.cloud2.position.set(160, -280);
            // this.checkMark1.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight());
            this.checkMark.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight());
            this.container.position.set(0.5*APP.GetWidth() - 50, 0.5*APP.GetHeight() + 300);
            this.optA.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight() + 250);
            this.optB.position.set(0.5*APP.GetWidth() + 100, 0.5*APP.GetHeight() + 250);
            this.arrowTut.position.set(0.5*APP.GetWidth() - 20, 0.5*APP.GetHeight() - 40);
        }
        else
        {
            this.mBackground.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 200);
            this.title.position.set(0.5*APP.GetWidth(), 300/GameDefine.SCALE_CANVAS);
            this.char1.position.set(20, -400);
            this.char2.position.set(200, -380);
            // this.checkMark1.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight() + 100);
            this.checkMark.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight() + 100);
            this.container.position.set(0.5*APP.GetWidth() - 100, 0.5*APP.GetHeight() + 300);
            this.cloud1.position.set(-260, -500);
            this.cloud2.position.set(160, -300);
            this.optA.position.set(0.5*APP.GetWidth() - 200, 0.5*APP.GetHeight() + 300);
            this.optB.position.set(0.5*APP.GetWidth() + 200, 0.5*APP.GetHeight() + 300);
            this.arrowTut.position.set(0.5*APP.GetWidth() - 50, 0.5*APP.GetHeight() - 200);
        }
        this.hand.position.set(0.25*APP.GetWidth(), APP.GetHeight() + 400);
        this.backgroundCheck.position.set(this.optA.position.x, this.optA.position.y);
        // this.checkMark2.position.set(this.checkMark1.position.x - 15, this.checkMark1.position.y - 5);
        this.checkMark.mask.position.set(this.checkMark.position.x, this.checkMark.position.y);
    }

    Scaling()
    {
        this.dark.clear();
        this.dark.beginFill(0x000000, 0.4);
        this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mBackground.scale.set(0.65, 0.25);
            this.char1.scale.set(0.65);
            this.char2.scale.set(0.65);
            this.tree.scale.set(0.45);
            this.optA.scale.set(0.5);
            this.optB.scale.set(0.5);
            this.cloud1.scale.set(0.5);
            this.cloud2.scale.set(0.5);
            this.backgroundCheck.scale.set(0.275/2);
            this.arrowTut.scale.set(0.3);
            this.hand.scale.set(0.75);
        }
        else
        {
            this.mBackground.scale.set(0.85, 0.45);
            this.char1.scale.set(0.9);
            this.char2.scale.set(0.9);
            this.tree.scale.set(0.75);
            this.backgroundCheck.scale.set(0.275);
            this.hand.scale.set(1.25/GameDefine.SCALE_CANVAS);
            this.arrowTut.scale.set(0.6);
            this.optA.scale.set(1);
            this.optB.scale.set(1);
        }
    }

    Update(dt)
    {
        this.updateCharAnimation(dt);
        this.updateTutHand(dt);
        // this.char2.rotation += 0.5*dt
        // this.mBackground.children[0].position.set(0, this.mBackground.children[1].height * this.mBackground.children[1].scale.y);
        if (this.checkMark
            && this.isWin
        )
            this.drawCheckMark(dt);
        if (this.isWin)
        {
            this.mCongratTimer.Update(dt);
            this.dark.alpha = Math.min(1, this.dark.alpha + 2*dt);
        }
        if (this.mCongratTimer.IsDone())
        {
            EventManager.publish(EventDefine.LEVEL_COMPLETED, {});
            // EventManager.publish(EventDefine.STATE_INGAME_COMPLETE, {});
        }
    }

    updateCharAnimation(dt)
    {
        if (this.char1)
        {
            // this.char1.head.rotation = (Math.PI/36)*Math.sin(this.char1.deltaTime) + Math.PI/36;
            this.char1.rotation = (Math.PI/48)*Math.sin(this.char1.deltaTime) + Math.PI/48;
            this.char1.legs.rotation = (Math.PI/12)*Math.sin(this.char1.deltaTime) + Math.PI/12;
            this.char1.deltaTime = Math.min(2*Math.PI, this.char1.deltaTime + 1*dt);
            if (this.char1.deltaTime == 2*Math.PI)
                this.char1.deltaTime = 0;
        }
        if (this.char2)
        {
            this.char2.rotation = (Math.PI/48)*Math.sin(this.char2.deltaTime) + Math.PI/48;
            this.char2.deltaTime = Math.min(2*Math.PI, this.char2.deltaTime + 1.5*dt);
            if (this.char2.deltaTime == 2*Math.PI)
                this.char2.deltaTime = 0;
        }
    }

    updateTutHand(dt)
    {
        if (this.hand.completePhase2)
            return;
        this.delayTut.Update(dt);
        if (this.delayTut.IsDone())
        {
            if (!this.hand.completePhase1)
            {
                let angle = Utils.Angle2Point(this.hand.position, {x: 0.5*APP.GetWidth() + 100, y: 0.5*APP.GetHeight() + 250});
                this.hand.position.x += 1600*Math.cos(angle)*dt;
                this.hand.position.y += 1600*Math.sin(angle)*dt;
                if (angle > 0)
                {
                    this.hand.completePhase1 = true;
                    this.hand.moveDir = 1;
                }
            }
            else
            {
                if (APP.GetWidth() > APP.GetHeight())
                {
                    this.hand.position.y = this.hand.position.y + (0.5*APP.GetHeight() - 30 - this.hand.position.y)*dt;
                    this.arrowTut.alpha = Math.min(1, this.arrowTut.alpha + 0.5*dt);
                    if (Math.abs(this.hand.position.y - (0.5*APP.GetHeight() - 30)) < 10)
                        this.hand.position.y = 0.5*APP.GetHeight() + 250;
                }
                else
                {
                    this.hand.position.y = this.hand.position.y + (0.5*APP.GetHeight() - 100 - this.hand.position.y)*dt;
                    this.arrowTut.alpha = Math.min(1, this.arrowTut.alpha + 0.5*dt);
                    if (Math.abs(this.hand.position.y - (0.5*APP.GetHeight() - 100)) < 10)
                        this.hand.position.y = 0.5*APP.GetHeight() + 250;
                    // this.hand.position.y = Math.min(0.5*APP.GetHeight() + 250, Math.max(0.5*APP.GetHeight() - 100, this.hand.position.y - 100*this.hand.moveDir*dt));
                    // if (this.hand.position.y == 0.5*APP.GetHeight() + 250 || this.hand.position.y == 0.5*APP.GetHeight() - 100)
                    //     this.hand.moveDir *= -1;
                }
            }
        }
    }

    drawCheckMark(dt)
    {
        this.checkMark.mask.position.set(this.checkMark.position.x, this.checkMark.position.y);
        this.checkMark.mask.scale.x = Math.min(1, this.checkMark.mask.scale.x + 2*dt);
    }

    TouchHandler(e)
    {
        if (this.isWin)
            return;
        if (Input.IsTouchDown(e))
        {
            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.container.mask.getBounds()
                )
            )
            {
                this.isTap = true;
                this.mTapPos = {
                    x: e.data.global.x,
                    y: e.data.global.y
                };
                if (this.hand.visible)
                {
                    this.hand.completePhase2 = true;
                    this.hand.visible = false;
                    this.arrowTut.visible = false;
                }
            }

            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.optA.getBounds()
                )
            )
            {
                this.tickSound.play();
                this.backgroundCheck.visible = true;
                this.isWin = true;
                if (this.hand.visible)
                {
                    this.hand.completePhase2 = true;
                    this.hand.visible = false;
                    this.arrowTut.visible = false;
                }
                ParticleMgr.SpawnParticle(0, GameDefine.CONFETTI_PARTICLE);
                EventManager.publish(EventDefine.LEVEL_CONGRAT, {});
            }
        }
        if (Input.IsTouchMove(e))
        {
            if (this.isTap)
            {
                let dy = e.data.global.y - this.mTapPos.y;
                if (APP.GetWidth() > APP.GetHeight())
                {
                    this.container.position.y = Math.max(0.5*APP.GetHeight() - 0, Math.min(0.5*APP.GetHeight() + 300, this.container.position.y + dy));
                    this.container.mask.scale.y = Math.max(0.25, Math.min(0.65, this.container.mask.scale.y - dy/1200));
                }
                else
                {
                    this.container.position.y = Math.max(0.5*APP.GetHeight() - 200, Math.min(0.5*APP.GetHeight() + 300, this.container.position.y + dy));
                    this.container.mask.scale.y = Math.max(0.45, Math.min(1, this.container.mask.scale.y - dy/1200));
                }
                this.mTapPos = {
                    x: e.data.global.x,
                    y: e.data.global.y
                };
            }
        }
        if (Input.IsTouchUp(e))
        {
            this.isTap = false;
        }
    }
}

module.exports = LevelSurvive;