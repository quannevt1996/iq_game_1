const GameDefine = require('../GameDefine');
const APP = require('../../app');
const Character = require('../Character');
const Input = require('../../core/Input');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Utils = require('../../core/Utils');
const Timer = require('../../core/Timer');
const ParticleMgr = require('../ParticleMgr');

//CHAR DEFINE
const CHAR1_ANCHOR_POSITION          = [{x: 0.53, y: 0.82}, {x: 0.47, y: 0.34}, {x: 0.55, y: 0.01}];
const CHAR1_BODY_RIG_POSITION        = [{x: 90, y: 2}, {x: 91, y: 124}];
const CHAR2_ANCHOR_POSITION          = [{x: 0.49, y: 0.92}, {x: 0.53, y: 0.33}, {x: 0.31, y: 0.02}];
const CHAR2_BODY_RIG_POSITION        = [{x: 99, y: 15}, {x: 99, y: 118}];
const CHAR3_ANCHOR_POSITION          = [{x: 0.54, y: 0.89}, {x: 0.55, y: 0.40}, {x: 0.47, y: 0.03}];
const CHAR3_BODY_RIG_POSITION        = [{x: 139, y: 30}, {x: 140, y: 185}];
const CHECK_MARK_POS                 = {left: {x: 0, y: 0}, mid: {x: 100, y: 100}, right: {x: 250, y: -50}};

class LevelImposter extends PIXI.Container
{
    constructor()
    {
        super();
        this.mIsFirstInit = false;
        this.mBackground = new PIXI.Container();
        this.char1 = null;
        this.char2 = null;
        this.char3 = null;
        this.table = null;
        this.bottle = null;
        this.title = null;
        this.isBottleTap = false;
        this.mTapPos = {x: null, y: null};
        this.isDroppingWater = false;
        // this.checkMark1 = new PIXI.Graphics();
        // this.checkMark2 = new PIXI.Graphics();
        this.curCheckMarkPos = {x: 0, y: 0};
        this.mCongratTimer = new Timer();
        this.dark = null;
        this.isLast = false;
        this.checkMark = null;
        this.hand = null;
        this.tutBottle = null;
        this.tutDelay = new Timer();
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: 'black',
                fontSize: '60px',
                lineJoin: "bevel",
                stroke: "black",
                strokeThickness: 1,
                fontWeight: 'bold'
            });
            const lineStyle  = {
                native: false,
                width: 30,
                color: 0x11b18f,
                alignment: 0.5,
                alpha: 1,
                join: 'round',
                lineCap: 'round',
                miterLimit: 198
            }
            //
            this.mIsFirstInit = true;
            //
            let t = new PIXI.Sprite(Resources.bgTop.texture)
            let b = new PIXI.Sprite(Resources.bgBottom.texture);
            // t.position.set(-0.5 * t.width, -t.height);
            // b.position.set(-0.5 * b.width, b.height);
            t.position.set(0, 0);
            b.position.set(3, t.height - 1);
            t.tint = 0xd3eef5;
            b.tint = 0x11b18f;
            b.scale.y = 1.25;
            this.mBackground.addChild(b, t);
            //
            this.title = new PIXI.Text('Who is an imposter?', style);
            this.title.anchor.set(0.5);
            this.table = new PIXI.Sprite(Resources.imposterTable.texture);
            this.table.anchor.set(0.5);
            this.bottle = new PIXI.Sprite(Resources.imposterBottle.texture);
            this.bottle.anchor.set(0.5);
            this.water = new PIXI.Sprite(Resources.imposterWater.texture);
            this.water.anchor.set(1, 0);
            this.water.visible = false;
            this.water.scale.set(0);
            this.dark = new PIXI.Graphics();
            this.dark.beginFill(0x000000, 0.4);
            this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
            this.dark.alpha = 0;
            this.checkMark = new PIXI.Sprite(Resources.correctTick.texture);
            let c = new PIXI.Graphics();
            c.beginFill(0x000000, 1);
            c.drawRect(0, 0, this.checkMark.width, this.checkMark.height);
            c.scale.x = 0;
            // this.checkMark.addChild(c);
            this.checkMark.mask = c;
            //
            this.hand = new PIXI.Sprite(Resources.tutHand.texture);
            this.hand.anchor.set(0.25, 0);
            this.tutBottle = new PIXI.Sprite(Resources.imposterBottle.texture);
            this.tutBottle.anchor.set(0.5);
            this.hand.alpha = 0;
            this.tutBottle.alpha = 0;
            this.tutDelay.SetDuration(2);
            // this.checkMark1.lineStyle(lineStyle);
            // this.checkMark1.roundPixels = true;
            // this.checkMark2.lineStyle(lineStyle);
            this.mCongratTimer.SetDuration(2);
            this.SetTouchable(true);
            this.InitCharacter();
            //
            this.addChild(this.mBackground);
            this.addChild(this.char1, this.char2, this.char3);
            this.addChild(this.table);
            this.addChild(this.bottle);
            this.addChild(this.water);
            // this.addChild(this.title);
            // this.addChild(this.dark);
            this.addChild(c);
            this.addChild(this.checkMark);
            // this.addChild(this.tutBottle);
            // this.addChild(this.checkMark1, this.checkMark2);
            this.Scaling();
            this.Positioning();
            this.EventHandler();
        }
    }

    Resize()
    {
        this.Scaling();
        this.Positioning();
    }

    AddUIParent()
    {
        this.parent.UIContainer.addChild(this.tutBottle);
        this.parent.UIContainer.addChild(this.hand);
        this.parent.UIContainer.addChild(this.title);
        this.parent.UIContainer.addChild(this.dark);
        if(GameDefine.SCALE_CANVAS != 1)
            this.hand.position.set(this.bottle.getGlobalPosition().x, this.bottle.getGlobalPosition().y - 0.5*this.bottle.height/GameDefine.SCALE_CANVAS);
        else
            this.hand.position.set(this.bottle.getGlobalPosition().x, this.bottle.position.y);
    }

    InitCharacter()
    {
        this.char1 = new Character({
            head: Resources.imposter_char1Head_before.texture,
            body: Resources.imposter_char1Body.texture,
            legs: Resources.imposter_char1Legs.texture,
            anchors: CHAR1_ANCHOR_POSITION,
            positions: CHAR1_BODY_RIG_POSITION
        });
        this.char1.isDrop = false;
        this.char1.deltaTime = 0;
        //
        this.char2 = new Character({
            head: Resources.imposter_char2Head_before.texture,
            body: Resources.imposter_char2Body.texture,
            legs: Resources.imposter_char2Legs.texture,
            anchors: CHAR2_ANCHOR_POSITION,
            positions: CHAR2_BODY_RIG_POSITION
        });
        this.char2.isDrop = false;
        this.char2.deltaTime = 0;
        //
        this.char3 = new Character({
            head: Resources.imposter_char3Head.texture,
            body: Resources.imposter_char3Body.texture,
            legs: Resources.imposter_char3Legs.texture,
            anchors: CHAR3_ANCHOR_POSITION,
            positions: CHAR3_BODY_RIG_POSITION
        });
        this.char3.isDrop = false;
        this.char3.deltaTime = 0;
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.ON_TOUCH, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_MOVE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_RELEASE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.STATE_INGAME_COMPLETE, function(){
            if (this.isLast)
                this.dark.visible = false;
        }.bind(this));
    }

    Positioning()
    {
        this.mBackground.position.set(0.5 * APP.GetWidth() - 0.5*this.mBackground.width, 0.5 * APP.GetHeight() - 0.5*this.mBackground.height);
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.title.position.set(0.5*APP.GetWidth(), 50);
            this.char1.position.set(0.5*APP.GetWidth()- 220, 0.5*APP.GetHeight() - 50);
            this.char2.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() - 50);
            this.char3.position.set(0.5*APP.GetWidth()+ 200, 0.5*APP.GetHeight() - 120);
            this.table.position.set(0.5*APP.GetWidth() - 400, 0.5*APP.GetHeight() + 250);
            this.bottle.position.set(this.table.position.x - 20, this.table.position.y - 170);
        }
        else
        {
            this.title.position.set(0.5*APP.GetWidth(), 200);
            this.char1.position.set(0.5*APP.GetWidth()- 140, 0.5*APP.GetHeight() - 100);
            this.char2.position.set(0.5*APP.GetWidth() + 65, 0.5*APP.GetHeight() - 100);
            this.char3.position.set(0.5*APP.GetWidth()+ 240, 0.5*APP.GetHeight() - 190);
            this.table.position.set(0.5*APP.GetWidth() - 300, 0.5*APP.GetHeight() + 250);
            this.bottle.position.set(this.table.position.x - 20, this.table.position.y - 170);
        }
        // this.hand.position.set(this.bottle.position.x+this.position.x, this.bottle.position.y+this.position.y);
        this.checkMark.position.set(this.char3.position.x, this.char3.position.y + 50);
        // this.checkMark1.position.set(this.char3.position.x, this.char3.position.y + 50);
        // this.checkMark2.position.set(this.checkMark1.position.x - 15, this.checkMark1.position.y - 5);
    }

    Scaling()
    {
        this.dark.clear();
        this.dark.beginFill(0x000000, 0.4);
        this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mBackground.scale.set(0.65);
            this.char1.scale.set(0.75);
            this.char2.scale.set(0.75);
            this.char3.scale.set(0.75);
            this.hand.scale.set(0.5);
        }
        else
        {
            this.mBackground.scale.set(0.85);
            this.char1.scale.set(0.9);
            this.char2.scale.set(0.9);
            this.char3.scale.set(0.9);
            this.hand.scale.set(0.75/GameDefine.SCALE_CANVAS);
            this.tutBottle.scale.set(1/GameDefine.SCALE_CANVAS);
        }
    }

    Update(dt)
    {
        this.updateCharAnimation(dt);
        this.updateTutHand(dt);
        if (this.bottle)
        {
            this.updateBottle(dt);
        }
        if (this.water)
        {
            this.updateWater(dt);
        }
        if (this.checkMark
            && this.char3.isDrop && this.water.alpha == 0)
            this.drawCheckMark(dt);
        if (this.char3.isDrop && this.water.alpha == 0)
        {
            this.dark.alpha = Math.min(1, this.dark.alpha + 1*dt);
            this.mCongratTimer.Update(dt);
        }
        if (this.mCongratTimer.IsDone())
        {
            EventManager.publish(EventDefine.LEVEL_COMPLETED, {});
        }
    }

    updateCharAnimation(dt)
    {
        if (this.char1)
        {
            this.char1.head.rotation = (Math.PI/36)*Math.sin(this.char1.deltaTime) + Math.PI/36;
            this.char1.body.rotation = (Math.PI/48)*Math.sin(this.char1.deltaTime) + Math.PI/48;
            this.char1.deltaTime = Math.min(2*Math.PI, this.char1.deltaTime + 1*dt);
            if (this.char1.deltaTime == 2*Math.PI)
                this.char1.deltaTime = 0;
        }
        if (this.char2)
        {
            this.char2.head.rotation = (Math.PI/48)*Math.sin(this.char2.deltaTime);
            // this.char1.body.rotation = (Math.PI/56)*Math.sin(this.char1.deltaTime) + Math.PI/56;
            this.char2.deltaTime = Math.min(2*Math.PI, this.char2.deltaTime + 3.5*dt);
            if (this.char2.deltaTime == 2*Math.PI)
                this.char2.deltaTime = 0;
        }
        if (this.char3)
        {
            this.char3.head.rotation = (Math.PI/36)*Math.sin(this.char3.deltaTime);
            // this.char1.body.rotation = (Math.PI/56)*Math.sin(this.char1.deltaTime) + Math.PI/56;
            this.char3.deltaTime = Math.min(2*Math.PI, this.char3.deltaTime + 2*dt);
            if (this.char3.deltaTime == 2*Math.PI)
                this.char3.deltaTime = 0;
        }
    }

    updateTutHand(dt)
    {
        this.tutBottle.position.set(this.hand.position.x, this.hand.position.y);
        if (!this.hand.visible)
            return;
        this.tutDelay.Update(dt);
        if (this.tutDelay.IsDone())
        {
            this.hand.alpha = Math.min(1, this.hand.alpha + 0.5*dt);
            this.tutBottle.alpha = Math.min(1, this.tutBottle.alpha + 0.5*dt);
            if (this.hand.alpha == 1)
            {
                if (!this.hand.completePhase1)
                {
                    let d = {x: this.char1.head.getBounds().x + 1*this.char1.head.getBounds().width, y: this.char1.head.getBounds().y};
                    this.hand.position.set(
                        this.hand.position.x + (d.x - this.hand.position.x)*dt,
                        this.hand.position.y + (d.y - this.hand.position.y)*dt
                    );
                    if (Utils.Distance2Point(this.hand.position, d) < 10)
                        this.hand.completePhase1 = true;
                }
                else
                {
                    if (!this.hand.completePhase2)
                    {
                        // let d = {x: this.char2.position.x + this.char2.head.position.x, y: this.char2.position.y + this.char2.head.position.y};
                        let d = {x: this.char2.head.getBounds().x + 1*this.char2.head.getBounds().width, y: this.char2.head.getBounds().y};
                        this.hand.position.set(
                            this.hand.position.x + (d.x - this.hand.position.x)*dt,
                            this.hand.position.y + (d.y - this.hand.position.y)*dt
                        );
                        if (Utils.Distance2Point(this.hand.position, d) < 10)
                            this.hand.completePhase2 = true;
                    }
                    else
                    {
                        // let d = {x: this.char3.position.x + this.char3.head.position.x, y: this.char3.position.y + this.char3.head.position.y};
                        let d = {x: this.char3.head.getBounds().x + 1*this.char3.head.getBounds().width, y: this.char3.head.getBounds().y};
                        this.hand.position.set(
                            this.hand.position.x + (d.x - this.hand.position.x)*dt,
                            this.hand.position.y + (d.y - this.hand.position.y)*dt
                        );
                        if (Utils.Distance2Point(this.hand.position, d) < 10)
                        {
                            this.hand.completePhase1 = false;
                            this.hand.completePhase2 = false;
                        }
                    }
                }
            }
        }
    }

    updateBottle(dt)
    {
        if (!this.isBottleTap)
        {
            if (APP.GetWidth() > APP.GetHeight())
            {
                this.bottle.position.set(
                    this.bottle.position.x + 5*(this.table.position.x - 20 - this.bottle.position.x)*dt, 
                    this.bottle.position.y + 5*(this.table.position.y - 170 - this.bottle.position.y)*dt
                );
            }
            else
            {
                this.bottle.position.set(
                    this.bottle.position.x + 5*(this.table.position.x - 20 - this.bottle.position.x)*dt, 
                    this.bottle.position.y + 5*(this.table.position.y - 170 - this.bottle.position.y)*dt
                );
            }
        }
    }

    updateWater(dt)
    {
        if (this.isDroppingWater)
        {
            this.bottle.rotation = Math.max(-Math.PI/2, this.bottle.rotation - 3.5*dt);
            if (this.bottle.rotation == -Math.PI/2)
            {
                this.isDroppingWater = false;
                this.water.position.set(this.bottle.position.x - 0.5*this.bottle.height, this.bottle.position.y);
                this.water.visible = true;
                this.water.alpha = 1;
                this.water.scale.set(0);
                if (this.char1.isDrop)
                    this.char1.head.texture = Resources.imposter_char1Head_after.texture;
                if (this.char2.isDrop)
                    this.char2.head.texture = Resources.imposter_char2Head_after.texture;
                if (this.char3.isDrop)
                {
                    this.char3.head.texture = Resources.imposter_char3Head_after.texture;
                    this.char3.body.texture = Resources.imposter_char3Body_after.texture;
                    ParticleMgr.SpawnParticle(0, GameDefine.CONFETTI_PARTICLE);
                    EventManager.publish(EventDefine.LEVEL_CONGRAT, {});
                }
            }
        }
        else
        {
            this.water.scale.set(Math.min(3, this.water.scale.x + 3*dt));
            this.water.alpha = Math.max(0,  this.water.alpha - 1*dt);
            this.bottle.rotation = Math.min(0, this.bottle.rotation + 5*dt);
        }
    }

    drawCheckMark(dt)
    {
        this.checkMark.mask.position.set(this.checkMark.position.x, this.checkMark.position.y);
        this.checkMark.mask.scale.x = Math.min(1, this.checkMark.mask.scale.x + 2*dt);
    }

    checkBottleFace(e)
    {
        if (!this.isDroppingWater && this.water.alpha == 0)
        {
            if (Utils.PointInRect(
                    {x: e.data.global.x,  y: e.data.global.y},
                    this.char1.head.getBounds()
                )
                && !this.char1.isDrop
                )
                {
                    this.isDroppingWater = true;
                    this.char1.isDrop = true;
                    this.water.alpha = 1;
                    this.water.scale.set(0);
                }
            if (Utils.PointInRect(
                    {x: e.data.global.x,  y: e.data.global.y},
                    this.char2.head.getBounds()
                    )
                && !this.char2.isDrop
                )
                {
                    this.isDroppingWater = true;
                    this.char2.isDrop = true;
                    this.water.alpha = 1;
                    this.water.scale.set(0);
                }
            if (Utils.PointInRect(
                    {x: e.data.global.x,  y: e.data.global.y},
                    this.char3.head.getBounds()
                )
                && !this.char3.isDrop
                )
                {
                    this.isDroppingWater = true;
                    this.char3.isDrop = true;
                    this.water.alpha = 1;
                    this.water.scale.set(0);
                }
        }
    }

    TouchHandler(e)
    {
        if (this.char3.isDrop && this.water.alpha == 0)
            return;
        if (Input.IsTouchDown(e))
        {
            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.bottle.getBounds()
                )
            )
            {
                this.isBottleTap = true;
                this.mTapPos = {
                    x: e.data.global.x,
                    y: e.data.global.y
                };
                this.hand.visible = false;
                this.tutBottle.visible = false;
            }
        }
        if (Input.IsTouchMove(e))
        {
            if (this.isBottleTap)
            {
                let dx = e.data.global.x - this.mTapPos.x;
                let dy = e.data.global.y - this.mTapPos.y;
                this.bottle.position.x += dx*GameDefine.SCALE_CANVAS;
                this.bottle.position.y += dy*GameDefine.SCALE_CANVAS;
                this.mTapPos = {
                    x: e.data.global.x,
                    y: e.data.global.y
                };
                
                this.checkBottleFace(e);
            }
        }
        if (Input.IsTouchUp(e))
        {
            this.isBottleTap = false;
        }
    }
}

module.exports = LevelImposter;