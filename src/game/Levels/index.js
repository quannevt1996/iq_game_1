const APP = require('../../app');
const EventDefine = require('../../events/EventDefine');
const EventManager = require('../../events/EventManager');
const LevelImposter = require('./LevelImposter');
const LevelWerewolf = require('./LevelWerewolf');
const LevelSurvive = require('./LevelSurvive');
const GameDefine = require('../GameDefine');

class Levels extends PIXI.Container
{
    constructor()
    {
        super();
        this.mLevels = [new LevelSurvive(), new LevelWerewolf(), new LevelImposter()];
        this.addChild(...this.mLevels);
        this.mCurLevel = 0;
        this.UIContainer = new PIXI.Container();
        this.addChild(this.UIContainer);
    }

    Init()
    {
        for (let i = 0; i< this.mLevels.length; i++)
        {
            this.mLevels[i].Init();
            this.mLevels[i].scale.set(1/GameDefine.SCALE_CANVAS);
            if (GameDefine.SCALE_CANVAS != 1)
                this.mLevels[i].position.set(
                    i * APP.GetWidth() + 0.5*APP.GetWidth() - 0.75*this.mLevels[i].width*this.mLevels[i].scale.x, 
                    0.5*APP.GetHeight() - 0.5*this.mLevels[i].height*this.mLevels[i].scale.y
                );
            else
            {
                this.mLevels[i].position.set(
                    i * APP.GetWidth(),
                    0.5*APP.GetHeight()
                );
            }
        }
        this.mLevels[this.mCurLevel].AddUIParent();
        this.mLevels[this.mLevels.length - 1].isLast = true;
        EventManager.subscribe(EventDefine.LEVEL_COMPLETED, function(){
            this.SetLevel(this.mCurLevel + 1);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_CONTAINER_RESIZE, function(){
            let l = Math.min(this.mCurLevel, this.mLevels.length - 1);
            for (let i = 0; i< this.mLevels.length; i++)
            {
                this.mLevels[i].scale.set(1/GameDefine.SCALE_CANVAS);
                if (GameDefine.SCALE_CANVAS != 1)
                this.mLevels[i].position.set(
                    this.mLevels[i].position.x + ((i - l)*APP.GetWidth() + 0.5*APP.GetWidth() - 0.75*this.mLevels[i].width*this.mLevels[i].scale.x), 
                    0.5*APP.GetHeight() - 0.75*this.mLevels[i].height*this.mLevels[i].scale.y
                );
                else
                {
                    this.mLevels[i].position.set(
                        this.mLevels[i].position.x + ((i - l)*APP.GetWidth()), 
                        0
                    );
                }
                this.mLevels[i].Resize();
            }
        }.bind(this));
    }

    SetLevel(l)
    {
        if (l >= this.mLevels.length)
        {
            EventManager.publish(EventDefine.STATE_INGAME_COMPLETE, {});
        }
        this.UIContainer.removeChildren();
        this.mCurLevel = l;
        if (l < this.mLevels.length) this.mLevels[this.mCurLevel].AddUIParent();
    }

    Update(dt)
    {
        if (this.mCurLevel >= this.mLevels.length)
            return;
        for (let i =0; i< this.mLevels.length; i++)
        {
            if (GameDefine.SCALE_CANVAS != 1)
                this.mLevels[i].position.set(
                    this.mLevels[i].position.x + 4 * ((i - this.mCurLevel)*APP.GetWidth() + 0.5*APP.GetWidth() - 0.75*this.mLevels[i].width*this.mLevels[i].scale.x - this.mLevels[i].position.x) * dt, 
                    0.5*APP.GetHeight() - 0.75*this.mLevels[i].height*this.mLevels[i].scale.y
                );
            else
            {
                this.mLevels[i].position.set(
                    this.mLevels[i].position.x + 4 * ((i - this.mCurLevel)*APP.GetWidth() - this.mLevels[i].position.x) * dt, 
                    0
                );
            }
        }
        this.mLevels[this.mCurLevel].Update(dt);
    }
}

module.exports = Levels;