const GameDefine = require('../GameDefine');
const APP = require('../../app');
const Character = require('../Character');
const Input = require('../../core/Input');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Utils = require('../../core/Utils');
const Timer = require('../../core/Timer');
const ParticleMgr = require('../ParticleMgr');

//CHAR DEFINE
const CHAR1_ANCHOR_POSITION          = [{x: 0.56, y: 0.71}, {x: 0.57, y: 0.49}, {x: 0.34, y: 0.01}];
const CHAR1_BODY_RIG_POSITION        = [{x: 148, y: 12}, {x: 145, y: 250}];
const CHAR2_ANCHOR_POSITION          = [{x: 0.48, y: 1}, {x: 0.56, y: 0.46}, {x: 0.46, y: 0.01}];
const CHAR2_BODY_RIG_POSITION        = [{x: 153, y: 25}, {x: 162, y: 246}];
const CHAR3_ANCHOR_POSITION          = [{x: 0.52, y: 1}, {x: 0.44, y: 0.52}, {x: 0.37, y: 0.02}];
const CHAR3_BODY_RIG_POSITION        = [{x: 130, y: 65}, {x: 115, y: 239}];
const CHECK_MARK_POS                 = {left: {x: 0, y: 0}, mid: {x: 100, y: 100}, right: {x: 250, y: -50}};

class LevelWerewolf extends PIXI.Container
{
    constructor()
    {
        super();
        this.mIsFirstInit = false;
        this.mBackground = new PIXI.Container();
        this.char1 = null;
        this.char2 = null;
        this.char3 = null;
        this.title = null;
        this.mountain = null;
        this.stick = null;
        this.potion = null;
        this.garlic = null;
        this.sun = null;
        this.moon = null;
        this.dark = null;
        this.initDist = null;
        this.isBottleTap = false;
        this.mTapPos = {x: null, y: null};
        this.isDroppingWater = false;
        // this.checkMark1 = new PIXI.Graphics();
        // this.checkMark2 = new PIXI.Graphics();
        this.checkMark =  null;
        this.curCheckMarkPos = {x: 0, y: 0};
        this.mCongratTimer = new Timer();
        this.mOnTarget = null;
        this.isWin = false;
        this.isLast = false;
        this.hand = null;
        this.arrowTut = null;
        this.tutDelay = new Timer();
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: 'black',
                fontSize: '60px',
                lineJoin: "bevel",
                stroke: "black",
                strokeThickness: 1,
                fontWeight: 'bold'
            });
            const lineStyle  = {
                native: false,
                width: 30,
                color: 0x11b18f,
                alignment: 0.5,
                alpha: 1,
                join: 'round',
                lineCap: 'round',
                miterLimit: 198
            }
            //
            this.mIsFirstInit = true;
            //
            let t = new PIXI.Sprite(Resources.bgTop.texture)
            let b = new PIXI.Sprite(Resources.bgBottom.texture);
            // t.position.set(-0.5 * t.width, -t.height);
            // b.position.set(-0.5 * b.width, b.height);
            t.position.set(0, 0);
            b.position.set(3, t.height - 1);
            t.tint = 0xd3eef5;
            b.tint = 0x11b18f;
            b.scale.y = 1.25;
            this.mBackground.addChild(b, t);
            //
            this.title = new PIXI.Text('Who is a werewolf?', style);
            this.title.anchor.set(0.5);
            this.mountain = new PIXI.Sprite(Resources.werewolfMountain.texture);
            this.mountain.anchor.set(0.5, 1);
            this.stick = new PIXI.Sprite(Resources.werewolfStick.texture);
            this.stick.anchor.set(0.5);
            this.stick.rotation = Math.PI/6;
            this.potion = new PIXI.Sprite(Resources.werewolfPotion.texture);
            this.potion.anchor.set(0.5);
            this.garlic = new PIXI.Sprite(Resources.werewolfGarlic.texture);
            this.garlic.anchor.set(0.5);
            this.sun = new PIXI.Sprite(Resources.werewolfSun.texture);
            this.sun.anchor.set(0.5);
            this.moon = new PIXI.Sprite(Resources.werewolfMoon.texture);
            this.moon.anchor.set(0.5);
            this.dark = new PIXI.Graphics();
            this.dark.beginFill(0x000000, 0.5);
            this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
            this.dark.alpha = 0;
            // this.checkMark1.lineStyle(lineStyle);
            // this.checkMark1.roundPixels = true;
            // this.checkMark2.lineStyle(lineStyle);
            this.checkMark = new PIXI.Sprite(Resources.correctTick.texture);
            let c = new PIXI.Graphics();
            c.beginFill(0x000000, 1);
            c.drawRect(0, 0, this.checkMark.width, this.checkMark.height);
            c.scale.x = 0;
            //
            this.hand = new PIXI.Sprite(Resources.tutHand.texture);
            this.hand.alpha = 0;
            this.arrowTut = new PIXI.Sprite(Resources.arrowTut.texture);
            this.arrowTut.anchor.set(0.5);
            this.arrowTut.alpha = 0;
            this.tutDelay.SetDuration(2);
            // this.checkMark.addChild(c);
            this.checkMark.mask = c;
            this.mCongratTimer.SetDuration(3);
            this.SetTouchable(true);
            this.InitCharacter();
            this.InitTapHandler();
            //
            this.addChild(this.mBackground);
            this.addChild(this.sun, this.moon);
            this.addChild(this.mountain);
            this.addChild(this.char1, this.char2, this.char3);
            this.addChild(this.stick, this.potion, this.garlic);
            
            this.addChild(this.checkMark);
            this.addChild(c);
            this.addChild(this.arrowTut);
            // this.addChild(this.checkMark1, this.checkMark2);
            this.Scaling();
            this.Positioning();
            this.EventHandler();
        }
    }

    Resize()
    {
        this.Scaling();
        this.Positioning();
    }

    AddUIParent()
    {
        this.parent.UIContainer.addChild(this.title);
        this.parent.UIContainer.addChild(this.dark);
        this.parent.UIContainer.addChild(this.hand);
    }

    InitCharacter()
    {
        this.char1 = new Character({
            head: Resources.werewolf_char1Head_before.texture,
            body: Resources.werewolf_char1Body.texture,
            legs: Resources.werewolf_char1Legs.texture,
            anchors: CHAR1_ANCHOR_POSITION,
            positions: CHAR1_BODY_RIG_POSITION
        });
        this.char1.addChild(this.char1.removeChild(this.char1.head));
        this.char1.isDrop = false;
        this.char1.deltaTime = 0;
        this.char1.onTargetCount = 0;
        this.char1.onDrop = function(){
            this.isDroppingWater = true;
            this.char1.isDrop = true;
            this.water.alpha = 1;
            this.water.scale.set(0);
        }.bind(this);
        //
        this.char2 = new Character({
            head: Resources.werewolf_char2Head_before.texture,
            body: Resources.werewolf_char2Body.texture,
            legs: Resources.werewolf_char2Legs.texture,
            anchors: CHAR2_ANCHOR_POSITION,
            positions: CHAR2_BODY_RIG_POSITION
        });
        this.char2.addChild(this.char2.removeChild(this.char2.head));
        this.char2.isDrop = false;
        this.char2.deltaTime = 0;
        this.char2.onTargetCount = 0;
        this.char2.onDrop = function(){
            this.isDroppingWater = true;
            this.char2.isDrop = true;
            this.water.alpha = 1;
            this.water.scale.set(0);
        }.bind(this);
        //
        this.char3 = new Character({
            head: Resources.werewolf_char3Head.texture,
            body: Resources.werewolf_char3Body.texture,
            legs: Resources.werewolf_char3Legs.texture,
            anchors: CHAR3_ANCHOR_POSITION,
            positions: CHAR3_BODY_RIG_POSITION
        });
        this.char3.addChild(this.char3.removeChild(this.char3.head));
        this.char3.isDrop = false;
        this.char3.deltaTime = 0;
        this.char3.onTargetCount = 0;
        this.char3.onDrop = function(){
            this.isDroppingWater = true;
            this.char3.isDrop = true;
            this.water.alpha = 1;
            this.water.scale.set(0);
        }.bind(this);
    }

    InitTapHandler()
    {
        this.garlic.onTap = function(e){
            let dx = e.data.global.x - this.mTapPos.x;
            let dy = e.data.global.y - this.mTapPos.y;
            this.garlic.position.x += dx*GameDefine.SCALE_CANVAS;
            this.garlic.position.y += dy*GameDefine.SCALE_CANVAS;
            this.mTapPos = {
                x: e.data.global.x,
                y: e.data.global.y
            };
        }.bind(this);
        this.potion.onTap = function(e){
            let dx = e.data.global.x - this.mTapPos.x;
            let dy = e.data.global.y - this.mTapPos.y;
            this.potion.position.x += dx*GameDefine.SCALE_CANVAS;
            this.potion.position.y += dy*GameDefine.SCALE_CANVAS;
            this.mTapPos = {
                x: e.data.global.x,
                y: e.data.global.y
            };
        }.bind(this);
        this.stick.onTap = function(e){
            let dx = e.data.global.x - this.mTapPos.x;
            let dy = e.data.global.y - this.mTapPos.y;
            this.stick.position.x += dx*GameDefine.SCALE_CANVAS;
            this.stick.position.y += dy*GameDefine.SCALE_CANVAS;
            this.mTapPos = {
                x: e.data.global.x,
                y: e.data.global.y
            };
        }.bind(this);
        this.sun.onTap = function(e){
            // let dx = e.data.global.x - this.mTapPos.x;
            let dy = e.data.global.y - this.mTapPos.y;
            // this.stick.position.x += dx;
            if (APP.GetWidth() > APP.GetHeight())
            {
                this.sun.position.y = Math.max(0.5*APP.GetHeight() - 180, Math.min( 0.5*APP.GetHeight() + 30, this.sun.position.y + dy));
                this.moon.position.y = Math.max(0.5*APP.GetHeight() - 180, Math.min( 0.5*APP.GetHeight() + 30, this.moon.position.y - dy));
            }
            else
            {
                this.sun.position.y = Math.max(0.5*APP.GetHeight() - 380, Math.min( 0.5*APP.GetHeight() , this.sun.position.y + dy));
                this.moon.position.y = Math.max(0.5*APP.GetHeight() - 380, Math.min( 0.5*APP.GetHeight() , this.moon.position.y - dy));
            }
            // console.log(d);
            this.mTapPos = {
                x: e.data.global.x,
                y: e.data.global.y
            };
        }.bind(this);
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.ON_TOUCH, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_MOVE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_RELEASE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.STATE_INGAME_COMPLETE, function(){
            if (this.isLast)
                this.dark.visible = false;
        }.bind(this));
    }

    Positioning()
    {
        
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mBackground.position.set(0.5 * APP.GetWidth() - 0.5*this.mBackground.width, 0.5 * APP.GetHeight() - 0.5*this.mBackground.height + 30);
            this.title.position.set(0.5*APP.GetWidth(), 50);
            this.char1.position.set(0.5*APP.GetWidth()- 280, 0.5*APP.GetHeight() - 20);
            this.char2.position.set(0.5*APP.GetWidth() - 70, 0.5*APP.GetHeight() - 20);
            this.char3.position.set(0.5*APP.GetWidth()+ 150, 0.5*APP.GetHeight() - 20);
            this.mountain.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 105);
            this.stick.position.set(0.5*APP.GetWidth()- 310, 0.5*APP.GetHeight() + 270);
            this.potion.position.set(0.5*APP.GetWidth()- 80, 0.5*APP.GetHeight() + 260);
            this.garlic.position.set(0.5*APP.GetWidth()+ 120, 0.5*APP.GetHeight() + 280);
            this.sun.position.set(0.5*APP.GetWidth()- 150, 0.5*APP.GetHeight() - 180);
            this.moon.position.set(0.5*APP.GetWidth()+ 10, 0.5*APP.GetHeight() + 30);
            this.initDist = Utils.Distance2Point(this.sun.position, {x: 0.5*APP.GetWidth()- 150, y: 0.5*APP.GetHeight()});
            this.arrowTut.position.set(this.sun.position.x, this.sun.position.y + 100);
        }
        else
        {
            this.mBackground.position.set(0.5 * APP.GetWidth() - 0.5*this.mBackground.width, 0.5 * APP.GetHeight() - 0.5*this.mBackground.height);
            this.title.position.set(0.5*APP.GetWidth(), 300/GameDefine.SCALE_CANVAS);
            this.char1.position.set(0.5*APP.GetWidth()- 450, 0.5*APP.GetHeight() - 150);
            this.char2.position.set(0.5*APP.GetWidth()- 115, 0.5*APP.GetHeight() - 150);
            this.char3.position.set(0.5*APP.GetWidth()+ 210, 0.5*APP.GetHeight() - 140);
            this.mountain.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 120);
            this.stick.position.set(0.5*APP.GetWidth()- 310, 0.5*APP.GetHeight() + 370);
            this.potion.position.set(0.5*APP.GetWidth()- 150, 0.5*APP.GetHeight() + 260);
            this.garlic.position.set(0.5*APP.GetWidth()+ 180, 0.5*APP.GetHeight() + 280);
            this.sun.position.set(0.5*APP.GetWidth()- 250, 0.5*APP.GetHeight() - 380);
            this.moon.position.set(0.5*APP.GetWidth()+ 250, 0.5*APP.GetHeight() - 50);
            this.initDist = Utils.Distance2Point(this.sun.position, {x: 0.5*APP.GetWidth()- 250, y: 0.5*APP.GetHeight() + 30});
            this.arrowTut.position.set(this.sun.position.x, this.sun.position.y + 150);
        }
        this.hand.position.set(this.sun.position.x*GameDefine.SCALE_CANVAS, this.sun.position.y*GameDefine.SCALE_CANVAS + this.position.y);
        this.checkMark.position.set(this.char3.position.x, this.char3.position.y + 50);
        // this.checkMark1.position.set(this.char3.position.x, this.char3.position.y + 50);
        // this.checkMark2.position.set(this.checkMark1.position.x - 15, this.checkMark1.position.y - 5);
    }

    Scaling()
    {
        this.dark.clear();
        this.dark.beginFill(0x000000, 0.4);
        this.dark.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.mBackground.scale.set(0.55);
            this.mountain.scale.set(0.65, 0.75);
            this.char1.scale.set(0.50);
            this.char2.scale.set(0.50);
            this.char3.scale.set(0.50);
            this.sun.scale.set(0.65);
            this.moon.scale.set(0.65);
            this.stick.scale.set(0.65);
            this.potion.scale.set(0.65);
            this.garlic.scale.set(0.65);
            this.hand.scale.set(0.45);
            this.arrowTut.scale.set(0.35, -0.25);
        }
        else
        {
            this.mBackground.scale.set(0.85);
            this.char1.scale.set(0.8);
            this.char2.scale.set(0.8);
            this.char3.scale.set(0.8);
            this.mountain.scale.set(1, 1.35);
            this.hand.scale.set(0.75/GameDefine.SCALE_CANVAS);
            this.arrowTut.scale.set(0.65, -0.45);
            this.sun.scale.set(1);
            this.moon.scale.set(1);
            this.stick.scale.set(1);
            this.potion.scale.set(1);
            this.garlic.scale.set(1);
        }
    }

    Update(dt)
    {
        this.updateCharAnimation(dt);
        this.updateTutHand(dt);
        this.updateObjectPosition(dt);
        this.updateDark(dt);
        this.updateWin(dt);
        if (this.sun)
        {
            this.sun.rotation = Math.min(2*Math.PI, this.sun.rotation + Math.PI/12*dt);
            if (this.sun.rotation == 2*Math.PI)
                this.sun.rotation = 0;
        }
        if (this.mOnTarget)
        {
            this.mOnTarget.onTargetCount += 1;
            if (this.mOnTarget >= 60 && !this.mOnTarget.isDrop)
            {
                this.mOnTarget.onDrop();
                this.mOnTarget = null;
            }
        }
        if (this.checkMark
            && this.isWin)
            this.drawCheckMark(dt);
        if (this.isWin)
            this.mCongratTimer.Update(dt);
        if (this.mCongratTimer.IsDone())
        {
            EventManager.publish(EventDefine.LEVEL_COMPLETED, {});
        }
    }

    updateCharAnimation(dt)
    {
        if (this.char1)
        {
            this.char1.head.rotation = (Math.PI/24)*Math.sin(this.char1.deltaTime) + Math.PI/24;
            // this.char1.body.rotation = (Math.PI/48)*Math.sin(this.char1.deltaTime) + Math.PI/48;
            this.char1.deltaTime = Math.min(2*Math.PI, this.char1.deltaTime + 2*dt);
            if (this.char1.deltaTime == 2*Math.PI)
                this.char1.deltaTime = 0;
        }
        if (this.char2)
        {
            this.char2.head.rotation = (Math.PI/24)*Math.sin(this.char2.deltaTime);
            this.char2.body.rotation = (Math.PI/112)*Math.sin(this.char2.deltaTime) + Math.PI/112;
            this.char2.deltaTime = Math.min(2*Math.PI, this.char2.deltaTime + 1*dt);
            if (this.char2.deltaTime == 2*Math.PI)
                this.char2.deltaTime = 0;
        }
        if (this.char3)
        {
            this.char3.head.rotation = (Math.PI/36)*Math.sin(this.char3.deltaTime);
            this.char3.body.rotation = (Math.PI/100)*Math.sin(this.char3.deltaTime) + Math.PI/100;
            this.char3.deltaTime = Math.min(2*Math.PI, this.char3.deltaTime + 2*dt);
            if (this.char3.deltaTime == 2*Math.PI)
                this.char3.deltaTime = 0;
        }
    }

    updateTutHand(dt)
    {
        if (!this.hand.visible)
            return;
        this.tutDelay.Update(dt);
        if (this.tutDelay.IsDone())
        {
            this.hand.alpha = Math.min(1, this.hand.alpha + 0.5*dt);
            if (this.hand.alpha == 1)
            {
                this.arrowTut.alpha = Math.min(1, this.arrowTut.alpha + 0.5*dt);
                this.hand.position.y = this.hand.position.y + (this.sun.position.y + 200/GameDefine.SCALE_CANVAS + this.position.y - this.hand.position.y)*dt;
                if (Math.abs(this.hand.position.y - (this.sun.position.y + 200/GameDefine.SCALE_CANVAS + this.position.y)) < 10)
                    this.hand.position.y = this.sun.position.y;
            }
        }
    }

    updateObjectPosition(dt)
    {
        if (this.mOnTarget)
            return;
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.stick.position.set(
                this.stick.position.x + 5*(0.5*APP.GetWidth()- 310 - this.stick.position.x)*dt, 
                this.stick.position.y + 5*(0.5*APP.GetHeight() + 270 - this.stick.position.y)*dt
            );
            this.potion.position.set(
                this.potion.position.x + 5*(0.5*APP.GetWidth()- 80 - this.potion.position.x)*dt, 
                this.potion.position.y + 5*(0.5*APP.GetHeight() + 260 - this.potion.position.y)*dt
            );
            this.garlic.position.set(
                this.garlic.position.x + 5*(0.5*APP.GetWidth()+ 120 - this.garlic.position.x)*dt, 
                this.garlic.position.y + 5*(0.5*APP.GetHeight() + 280 -this.garlic.position.y)*dt
            );
            if (!this.isWin)
            {
                this.sun.position.set(
                    0.5*APP.GetWidth()- 150, 
                    this.sun.position.y + 5*(0.5*APP.GetHeight() - 180 - this.sun.position.y)*dt
                );
                this.moon.position.set(
                    0.5*APP.GetWidth()+ 150, 
                    this.moon.position.y + 5*(0.5*APP.GetHeight() + 30 - this.moon.position.y)*dt
                );
            }
            else
            {
                this.sun.position.set(
                    0.5*APP.GetWidth()- 150, 
                    this.sun.position.y + 5*(0.5*APP.GetHeight() + 30 - this.sun.position.y)*dt
                );
                this.moon.position.set(
                    0.5*APP.GetWidth()+ 150, 
                    this.moon.position.y + 5*(0.5*APP.GetHeight() - 180 - this.moon.position.y)*dt
                );
            }
        }
        else
        {
            this.stick.position.set(
                this.stick.position.x + 5*(0.5*APP.GetWidth()- 310 - this.stick.position.x)*dt, 
                this.stick.position.y + 5*(0.5*APP.GetHeight() + 370 - this.stick.position.y)*dt
            );
            this.potion.position.set(
                this.potion.position.x + 5*(0.5*APP.GetWidth()- 150 - this.potion.position.x)*dt, 
                this.potion.position.y + 5*(0.5*APP.GetHeight() + 260 - this.potion.position.y)*dt
            );
            this.garlic.position.set(
                this.garlic.position.x + 5*(0.5*APP.GetWidth()+ 180 - this.garlic.position.x)*dt, 
                this.garlic.position.y + 5*(0.5*APP.GetHeight() + 280 - this.garlic.position.y)*dt
            );
            if (!this.isWin)
            {
                this.sun.position.set(
                    0.5*APP.GetWidth()- 250, 
                    this.sun.position.y + 5*(0.5*APP.GetHeight() - 380 - this.sun.position.y)*dt
                );
                this.moon.position.set(
                    0.5*APP.GetWidth()+ 250, 
                    this.moon.position.y + 5*(0.5*APP.GetHeight() - 50 -this.moon.position.y)*dt
                );
            }
            else
            {
                this.sun.position.set(
                    0.5*APP.GetWidth()- 250, 
                    this.sun.position.y + 5*(0.5*APP.GetHeight() - this.sun.position.y)*dt
                );
                this.moon.position.set(
                    0.5*APP.GetWidth()+ 250, 
                    this.moon.position.y + 5*(0.5*APP.GetHeight() - 380 -this.moon.position.y)*dt
                );
            }
        }
    }

    updateDark(dt)
    {
        let d ;
        if (APP.GetWidth() > APP.GetHeight())
        {
            d = Utils.Distance2Point(this.sun.position, {x: this.sun.position.x, y: 0.5*APP.GetHeight() + 30});
        }
        else
        {
            d = Utils.Distance2Point(this.sun.position, {x: this.sun.position.x, y: 0.5*APP.GetHeight()});
        }
        if (this.dark)
            this.dark.alpha = 1 - (d/this.initDist);
    }

    updateWin(dt)
    {
        let d ;
        if (APP.GetWidth() > APP.GetHeight())
        {
            d = Utils.Distance2Point(this.sun.position, {x: this.sun.position.x, y: 0.5*APP.GetHeight() + 30});
            if (d < 80 && !this.isWin)
            {
                this.winHandler();
            }
        }
        else
        {
            d = Utils.Distance2Point(this.sun.position, {x: this.sun.position.x, y: 0.5*APP.GetHeight()});
            if (d < 180 && !this.isWin)
            {
                this.winHandler();
            }
        }
    }

    winHandler()
    {
        this.isWin = true;
        this.mOnTarget = null;
        this.addChild(this.removeChild(this.moon));
        this.char1.head.texture = Resources.werewolf_char1Head_after.texture;
        this.char2.head.texture = Resources.werewolf_char2Head_after.texture;
        this.char3.removeChildren();
        let c = new PIXI.Sprite(Resources.werewolf_char3Full.texture);
        c.position.y -= 115;
        this.char3.addChild(c);
        ParticleMgr.SpawnParticle(0, GameDefine.CONFETTI_PARTICLE);
        EventManager.publish(EventDefine.LEVEL_CONGRAT, {});
    }

    drawCheckMark(dt)
    {
        this.checkMark.mask.position.set(this.checkMark.position.x, this.checkMark.position.y);
        this.checkMark.mask.scale.x = Math.min(1, this.checkMark.mask.scale.x + 2*dt);
    }

    TouchHandler(e)
    {
        if (this.isWin)
            return;
        if (Input.IsTouchDown(e))
        {
            this.mTapPos = {
                x: e.data.global.x,
                y: e.data.global.y
            };
            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.garlic.getBounds()
                )
            )
            {
                this.mOnTarget = this.garlic;
                return;
            }
            if (Utils.PointInRect(
                {x: e.data.global.x, y: e.data.global.y}, 
                this.potion.getBounds()
                )
            )
            {
                this.mOnTarget = this.potion;
                return;
            }
            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.stick.getBounds()
                )
            )
            {
                this.mOnTarget = this.stick;
                return;
            }
            if (Utils.PointInRect(
                    {x: e.data.global.x, y: e.data.global.y}, 
                    this.sun.getBounds()
                )
            )
            {
                this.hand.visible = false;
                this.arrowTut.visible = false;
                this.mOnTarget = this.sun;
                return;
            }
            this.mOnTarget = null;
        }
        if (Input.IsTouchMove(e))
        {
            if (this.mOnTarget)
                this.mOnTarget.onTap(e);
        }
        if (Input.IsTouchUp(e))
        {
            this.mOnTarget = null;
        }
    }
}

module.exports = LevelWerewolf;