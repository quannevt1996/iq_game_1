const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');
const GameDefine = require('./GameDefine');
const ParticleMgr = require('./ParticleMgr');

class ComboMgr extends PIXI.Container
{
    constructor()
    {
        super();
        this.mState = null;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        n = 0;
        this.COMBO = {
            AMAZING: n++,
            PERFECT: n++
        };
        this.TEXTURES = [
            Resources.greatCombo.texture,
            Resources.perfectCombo.texture
        ];
        this.mComboCount = 0;
        this.mComboSprite = null;
        this.mPrevCombo = null;
        this.mComboText = null;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.INIT:
            break;

            case this.STATE.RUN:
                if (this.mComboSprite)
                {
                    this.mComboSprite.scaleDelta = Math.min(this.mComboSprite.scaleDelta + 20*dt, 2.25*Math.PI);
                    this.mComboSprite.scale.set(Math.cos(this.mComboSprite.scaleDelta) + 1);
                }
            break;
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                const style = new PIXI.TextStyle({
                    fontFamily: GameDefine.MAIN_FONT.name,
                    fill: 'white',
                    fontSize: 30,
                    lineJoin: "bevel",
                    stroke: "black",
                    strokeThickness: 1
                });
                this.mComboSprite = new PIXI.Sprite();
                this.mComboSprite.anchor.set(0.5);
                this.mComboSprite.scaleDelta = 2.25*Math.PI;
                this.mComboSprite.scale.set(0);
                this.mComboText = new PIXI.Text('', style);
                this.mComboText.anchor.set(1, 0.5);
                this.mComboText.position.set(75, 35);
                EventManager.subscribe(EventDefine.ON_SCORE_SUCCESS, function(data){
                    if (data.y > GameDefine.NOTE_BASE_Y + 200 || data.y < GameDefine.NOTE_BASE_Y - 150)
                    {
                        //GREAT
                        this.mComboCount = (this.mPrevCombo == this.COMBO.AMAZING) ? this.mComboCount + 1 : 0;
                        this.mComboSprite.texture = this.TEXTURES[this.COMBO.AMAZING];
                        this.mPrevCombo = this.COMBO.AMAZING;
                    }
                    else
                    {
                        //PERFECT
                        this.mComboCount = (this.mPrevCombo == this.COMBO.PERFECT) ? this.mComboCount + 1 : 0;
                        this.mComboSprite.texture = this.TEXTURES[this.COMBO.PERFECT];
                        this.mPrevCombo = this.COMBO.PERFECT;
                        ParticleMgr.SpawnPerfectParticle(this.position);
                    }
                    this.mComboSprite.scaleDelta = Math.PI;
                    this.mComboText.text = this.mComboCount == 0 ? '' : `x ${this.mComboCount}`;
                }.bind(this));
                //
                this.mComboSprite.addChild(this.mComboText);
                this.addChild(this.mComboSprite);
                this.Start();
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = ComboMgr;