const Utils = require("../../core/Utils");
const APP = require('../../app');

class Confetti extends PIXI.Container
{
    constructor()
    {
        super();
        this.mObject = null;
        this.pieces = (function(){
            let arr = [];
            for (let i = 0; i<5; i++)
                arr.push(Resources[`confetti${i}`].texture);
            return arr;
        }());
        this.mSpeed = 0;
        this.mRotateSpeed = 0;
        let n = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            INACTIVE: n++,
        };
        this.isFirstInit = false;
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                this.position.y += this.mSpeed * dt;
                this.rotation += this.mRotateSpeed * dt;
                this._UpdateInactive();
            break;
        }
    }

    IsInactive()
    {
        return this.mState == this.STATE.INACTIVE;
    }

    _UpdateInactive()
    {
        if (this.position.y > APP.GetHeight())
            this._SetState(this.STATE.INACTIVE);
    }

    _SetState(s)
    {
        this.mState = s;
        switch (this.mState)
        {
            case this.STATE.INIT:
                if (!this.isFirstInit)
                {
                    this.isFirstInit = true;
                    this.mObject = new PIXI.Sprite(this.pieces[Utils.RandInt(0, this.pieces.length - 1)]);
                    this.mObject.anchor.set(0.5);
                    this.mObject.scale.set(0.5);
                    this.addChild(this.mObject);
                }
            break;

            case this.STATE.RUN:
                this.visible = true;
                this.mSpeed = Utils.Rand(500, 1000);
                this.mRotateSpeed = Utils.Rand(Math.PI/6, Math.PI/0.75);
                this.rotation = 0;
            break;

            case this.STATE.INACTIVE:
                this.visible = false;
            break;
        }
    }
}

module.exports = Confetti;