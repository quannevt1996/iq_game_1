class Character extends PIXI.Container
{
    constructor(opts = {head: null, body: null, legs: null})
    {
        super();
        this.head = new PIXI.Sprite(opts.head);
        this.body = new PIXI.Sprite(opts.body);
        this.legs = new PIXI.Sprite(opts.legs);
        this.face = new PIXI.Sprite(opts.face);
        //
        // this.head.anchor.set(0.5, 1);
        // this.body.anchor.set(0.5);
        // this.legs.anchor.set(0.5, 0);
        //
        this.SetPartAnchor(opts.anchors);
        this.SetPartPosition(opts.positions);
        this.addChild(this.head, this.legs, this.body);
    }

    Update(dt)
    {
        
    }

    SetPartAnchor(i)
    {
        this.head.anchor.set(i[0].x, i[0].y);
        this.body.anchor.set(i[1].x, i[1].y);
        this.legs.anchor.set(i[2].x, i[2].y);
    }

    SetPartPosition(i)
    {
        this.head.position.set(i[0].x, i[0].y);
        this.body.position.set(this.body.anchor.x*this.body.width, this.body.anchor.y*this.body.height);
        this.legs.position.set(i[1].x, i[1].y);
    }
}

module.exports = Character;