class GameDefine
{
    constructor()
    {
        //CHEAT
        this.CHEAT_IMMORTAL                 = false;
        this.CHEAT_AUTO                     = false;

        let n = 0;
        this.GAME_STATE_PRELOAD             = n++;
        this.GAME_STATE_LOADING             = n++;
        this.GAME_STATE_INGAME              = n++;
        this.GAME_STATE_END                 = n++;

        //FONT
        this.MAIN_FONT                      = {name: "patrick-hand", type: 'woff'};

        //OTHER
        this.RANK_BAR_LENGTH                = 85; //PERCENTAGE
        this.TOTAL_RANK                     = 6;
        this.RANK_TINT_COLOR                = 0xfcc603;
        this.GAME_LOSE_BACK_TIMER           = 0.5;

        //PARTICLE
        n = 0;
        // this.PERFECT_PARTICLE               = n++;
        this.CONFETTI_PARTICLE              = n++;

        //WIN/LOSE/OTHER CONDITION
        n = 0;
        this.IS_GAME_WIN                    = n++;
        this.IS_GAME_LOSE                   = n++;

        this.REDIRECT_URL                   = 'https://play.google.com/store/apps/details?id=com.unicostudio.whois&hl=en_US&gl=US';
        
        //
        this.KeyHandler();
    }

    /**
     * 
     * @param {CanvasContainer} container contain container width and height after resize, use this for responsive purpose 
     */
    Resize(container)
    {
        if (container.GetWidth() > container.GetHeight() || container.GetWidth() >= 900)
            this.SCALE_CANVAS = 1;
        else
            this.SCALE_CANVAS = container.maxRatio/(container.GetWidth()/container.GetHeight());
        console.log(this.SCALE_CANVAS);
        // console.log(this.SCALE_CANVAS);
        // if (container.width > container.height)
        // {
        //     this.NOTE_BASE_SPEED                = 2400 * container.maxRatio/(container.GetWidth()/container.GetHeight());
        //     this.NOTE_NORMAL_HEIGHT             = 350 * container.maxRatio/(container.GetWidth()/container.GetHeight());
        // }
        // this.NOTE_BASE_Y                    = 1000 * container.GetHeight()/1334;
        // console.log(this.NOTE_NORMAL_HEIGHT);
    }

    KeyHandler()
    {
        document.addEventListener("keydown", function(event) {
            let d = document.getElementById('debugger');
            let s;
            if (!d)
            {
                console.log('no dd');
                d = document.createElement('div');
                d.id = 'debugger';
                d.style.position = 'fixed';
                d.style.top = 0;
                d.style.right = 0;
                document.body.appendChild(d);
            }
            if (event.keyCode === 73) {
                this.CHEAT_IMMORTAL = !this.CHEAT_IMMORTAL;
                if (d)
                {
                    s = document.createElement('span');
                    s.innerHTML = this.CHEAT_IMMORTAL ? 'You are immortal' : 'You can be die';
                    s.style.fontSize = '30px';
                    d.appendChild(s);
                    d.appendChild(document.createElement('br'));
                }
            }
            if (event.keyCode === 65) {
                this.CHEAT_AUTO = !this.CHEAT_AUTO;
                if (d)
                {
                    s = document.createElement('span');
                    s.innerHTML = this.CHEAT_AUTO ? 'Auto play Enabled' : 'Auto play disabled';
                    s.style.fontSize = '30px';
                    d.appendChild(s);
                    d.appendChild(document.createElement('br'));
                }
            }
            // do something
        }.bind(this));
    }
}

module.exports = new GameDefine();