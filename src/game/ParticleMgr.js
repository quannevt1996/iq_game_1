const ObjectMgr = require('./ObjectMgr');
const Confetti = require('./Particles/Confetti');
const GameDefine = require('./GameDefine');
const Utils = require('../core/Utils');
const APP = require('../app');

class ParticleMgr extends PIXI.Container
{
    constructor()
    {
        super();
        let n = 0;
        this.mState = null;
        this.STATE = {
            INIT: n++,
            RUN: n++
        };
        this.mConfettiParticles = new ObjectMgr(Confetti);
    }

    Init()
    {
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch(this.mState)
        {
            case this.STATE.RUN:
                this.mConfettiParticles.Update(dt);
            break;
        }
    }

    GetParticleContainer(c)
    {
        switch (c)
        {
            case GameDefine.CONFETTI_PARTICLE:
                return this.mConfettiParticles;
        }
    }

    SpawnParticle(position, type)
    {
        // let particle = this.mPerfectParticles._GetInstance();
        // particle.Init();
        // particle.position.set(position.x, position.y);
        // particle.Start();
        // console.log(GameDefine.CONFETTI_PARTICLE);
        switch (type)
        {
            case GameDefine.CONFETTI_PARTICLE:
                // console.log('here');
                for (let i = 0; i< 50; i++)
                {
                    // console.log('spawn');
                    let particle = this.mConfettiParticles._GetInstance();
                    particle.Init();
                    particle.position.set(Utils.Rand(0, APP.GetWidth()), Utils.Rand(0, -APP.GetHeight()));
                    particle.Start();
                }
            break;
        }
    }

    _SetState(s)
    {
        this.mState = s;
        switch (s)
        {
            case this.STATE.INIT:
                // this.addChild(this.mPerfectParticles);
                console.log(this);
            break;

            case this.STATE.RUN:
                // console.log('start');
            break;
        }
    }
}

const p = new ParticleMgr();
p.Init();

module.exports = p;