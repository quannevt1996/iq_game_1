class Override
{
    constructor()
    {
        PIXI.Sprite.prototype.addText = function(text, style = {})
        {
            this.textContainer = this.textContainer ? this.textContainer : [];
            let t = new PIXI.Text(text, style);
            // this.textContainer = new PIXI.Text(text, style);
            t.anchor.set(0.5, 0.5);
            this.textContainer.push(t);
            this.addChild(t);
            return t;
        }

        PIXI.Sprite.prototype.SetTouchable = function(i = false)
        {
            this.interactive = i;
            this.buttonMode = i;
            // this
            //     // set the mousedown and touchstart callback...
            //     .on('mousedown', this.TouchHandler)
            //     .on('touchstart', this.TouchHandler)
            //     .on('mousemove', this.TouchHandler)
            //     .on('pointerdown', this.TouchHandler)
            //     .on('pointermove', this.TouchHandler)
            //     .on('pointerup', this.TouchHandler)

            //     // set the mouseup and touchend callback...
            //     .on('mouseup', this.TouchHandler)
            //     .on('touchend', this.TouchHandler)
            //     .on('tap', this.TouchHandler)
        }

        PIXI.Sprite.prototype.SetAlignment = function(a = {verticle: 'top', horizontal: 'left'})
        {
            switch(a.verticle)
            {
                case 'top':
                break;
    
                case 'middle':
                    this.position.y -= 0.5*this.height
                break;
    
                case 'bottom':
                    this.position.y -= this.height;
                break;
            }
    
            switch(a.horizontal)
            {
                case 'left':
                break;
    
                case 'middle':
                    this.position.x -= 0.5*this.width;
                break;
    
                case 'right':
                    this.position.x -= this.width;
                break;
            }
        }

        PIXI.Sprite.prototype.bringToFront = function() {	
            if (this.parent) 
            {		
                var parent = this.parent;		
                parent.removeChild(this);		
                parent.addChild(this);	
            }
        }

        PIXI.Container.prototype.bringToFront = function() {	
            if (this.parent) 
            {		
                var parent = this.parent;		
                parent.removeChild(this);		
                parent.addChild(this);	
            }
        }

        PIXI.Container.prototype.SetTouchable = function(i = false)
        {
            this.interactive = true;
            switch (i)
            {
                case true: 
                    // this.hitArea = new PIXI.Rectangle(0,0, this.width, this.height);
                    let bound = this.getLocalBounds();
                    this.hitArea = new PIXI.Rectangle(bound.x, bound.y, bound.width, bound.height);
                    this
                        // set the mousedown and touchstart callback...
                        .on('mousedown', this.TouchHandler)
                        .on('touchstart', this.TouchHandler)
                        // .on('mousemove', this.TouchHandler)
                        // .on('pointerdown', this.TouchHandler)
                        .on('pointermove', this.TouchHandler)
                        // .on('pointerup', this.TouchHandler)

                        // set the mouseup and touchend callback...
                        .on('mouseup', this.TouchHandler)
                        .on('touchend', this.TouchHandler)
                break;

                case false:
                    this.hitArea = null;
                    this.removeAllListeners();
                break;
            }
        }
    }
}

module.exports = new Override();
