class Timer
{
    constructor()
    {
        this.mTimer = 0;
        this.mIsDone = false;
        this.mDuration = 0;
        this.mIsStop = false;
        this.mCallback = null;
        this.mTimestampEvents = [];
    }

    SetDuration(d)
    {
        this.mDuration = d;
    }

    GetTime()
    {
        return Math.round(this.mTimer * 100)/100;
    }

    Reset()
    {
        this.mIsDone = false;
        this.mTimer = 0;
    }

    IsDone()
    {
        return this.mIsDone;
    }

    Stop()
    {
        this.mIsStop = true;
    }
    
    Resume()
    {
        this.mIsStop = false;
    }

    SetDoneCallback(c)
    {
        this.mCallback = c;
    }

    /**
     * 
     * @param {Number} ts Time when the callback should be called 
     * @param {*} c call back for the timestamp setted 
     */
    RegisterTimestampCallback(ts , c)
    {
        let e = {
            timestamp: ts,
            callback: c,
            isCall: false
        };
        this.mTimestampEvents.push(e);
    }

    Update(dt)
    {
        if (this.mIsDone || this.mIsStop)
            return;
        if (this.mTimer >= this.mDuration)
        {
            this.mIsDone = true;
            // this.mTimer = this.mDuration;
            if (this.mCallback)
                this.mCallback && this.mCallback();
            return;
        }
        this._UpdateTimestamp();
        this.mTimer += dt;
    }

    _UpdateTimestamp()
    {
        if (this.mTimestampEvents.length == 0 || this.mTimer == 0)
            return;
        for (let i = 0; i< this.mTimestampEvents.length; i++)
        {
            if (this.mTimestampEvents[i].timestamp > this.mTimer)
                continue;
            else
            {
                if (!this.mTimestampEvents[i].isCall)
                {
                    this.mTimestampEvents[i].isCall = true;
                    this.mTimestampEvents[i].callback && this.mTimestampEvents[i].callback();
                }
            }
        }
    }
}

module.exports = Timer;