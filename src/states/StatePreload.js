const BaseState = require('./BaseState');
const View = require('../views/StatePreloadView');
const APP = require('../app');
const Utils = require('../core/Utils');
const GameDefine = require('../game/GameDefine');
const spritesheet = require('../assets/images/sprites.json');
const StateManager = require('../states/StateManager');

class StatePreload extends BaseState
{
    constructor()
    {
        super();
        // console.log(new StateManager);
        //
        this.mView = new View();
    }

    Prelease()
    {
        // console.log('add');
        APP.stage.addChild(this.mView);
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            let assets = [];
            // assets.push({name: 'introBg', url: Utils.GetAssetUrl('images/intro/bg.jpg')});
            assets.push({name: 'bgTop', url: Utils.GetAssetUrl('images/gameplay/bgtop.png')});
            assets.push({name: 'bgBottom', url: Utils.GetAssetUrl('images/gameplay/bgbottom.png')});
            //Imposter//////////////////
            //Char2
            assets.push({name: 'imposter_char2Head_before', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_1/Before_Head.png')});
            assets.push({name: 'imposter_char2Head_after', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_1/After_Head.png')});
            assets.push({name: 'imposter_char2Body', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_1/Body.png')});
            assets.push({name: 'imposter_char2Legs', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_1/Legs.png')});
            //Char1
            assets.push({name: 'imposter_char1Head_before', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_2/Before_Head.png')});
            assets.push({name: 'imposter_char1Head_after', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_2/After_Head.png')});
            assets.push({name: 'imposter_char1Body', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_2/Body.png')});
            assets.push({name: 'imposter_char1Legs', url: Utils.GetAssetUrl('images/gameplay/Imposter/Woman_2/Legs.png')});
            //Char3
            assets.push({name: 'imposter_char3Head', url: Utils.GetAssetUrl('images/gameplay/Imposter/Man_woman/Woman_Head.png')});
            assets.push({name: 'imposter_char3Head_after', url: Utils.GetAssetUrl('images/gameplay/Imposter/Man_woman/After_Head.png')});
            assets.push({name: 'imposter_char3Body', url: Utils.GetAssetUrl('images/gameplay/Imposter/Man_woman/Body.png')});
            assets.push({name: 'imposter_char3Body_after', url: Utils.GetAssetUrl('images/gameplay/Imposter/Man_woman/After_Body.png')});
            assets.push({name: 'imposter_char3Legs', url: Utils.GetAssetUrl('images/gameplay/Imposter/Man_woman/Legs.png')});
            //
            assets.push({name: 'imposterBottle', url: Utils.GetAssetUrl('images/gameplay/Imposter/Bottle.png')});
            assets.push({name: 'imposterTable', url: Utils.GetAssetUrl('images/gameplay/Imposter/Table.png')});
            assets.push({name: 'imposterWater', url: Utils.GetAssetUrl('images/gameplay/Imposter/Water.png')});
            /////////////////////////////

            //Werewolf//////////////////
            //Char1
            assets.push({name: 'werewolf_char1Head_before', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_1/Before_Head.png')});
            assets.push({name: 'werewolf_char1Head_after', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_1/After_Head.png')});
            assets.push({name: 'werewolf_char1Body', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_1/Body.png')});
            assets.push({name: 'werewolf_char1Legs', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_1/Legs.png')});
            //Char2
            assets.push({name: 'werewolf_char2Head_before', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_2/Before_Head.png')});
            assets.push({name: 'werewolf_char2Head_after', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_2/After_Head.png')});
            assets.push({name: 'werewolf_char2Body', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_2/Body.png')});
            assets.push({name: 'werewolf_char2Legs', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Man_2/Legs.png')});
            //Char3
            assets.push({name: 'werewolf_char3Head', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Wolf/Head.png')});
            assets.push({name: 'werewolf_char3Body', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Wolf/Body.png')});
            assets.push({name: 'werewolf_char3Legs', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Wolf/Legs.png')});
            assets.push({name: 'werewolf_char3Full', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Wolf/FullBody.png')});
            //
            assets.push({name: 'werewolfMountain', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Mountain.png')});
            assets.push({name: 'werewolfSun', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Sun.png')});
            assets.push({name: 'werewolfMoon', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Moon.png')});
            assets.push({name: 'werewolfGarlic', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Garlic.png')});
            assets.push({name: 'werewolfPotion', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Potion.png')});
            assets.push({name: 'werewolfStick', url: Utils.GetAssetUrl('images/gameplay/Werewolf/Wooden_Stake.png')});
            /////////////////////////////

            //Survive//////////////////
            //Char1
            assets.push({name: 'survive_char1Body', url: Utils.GetAssetUrl('images/gameplay/Survive/man_1/Body.png')});
            assets.push({name: 'survive_char1Legs', url: Utils.GetAssetUrl('images/gameplay/Survive/man_1/Legs.png')});
            //Char2
            assets.push({name: 'survive_char2Body', url: Utils.GetAssetUrl('images/gameplay/Survive/man_2/Body.png')});
            assets.push({name: 'survive_char2Legs', url: Utils.GetAssetUrl('images/gameplay/Survive/man_2/Legs.png')});
            //
            assets.push({name: 'surviveTree', url: Utils.GetAssetUrl('images/gameplay/Survive/Tree.png')});
            assets.push({name: 'surviveCloud', url: Utils.GetAssetUrl('images/gameplay/Survive/Cloud.png')});
            assets.push({name: 'surviveMask', url: Utils.GetAssetUrl('images/gameplay/Survive/Mask.png')});
            /////////////////////////////
            assets.push({name: 'gameplay_optA', url: Utils.GetAssetUrl('images/gameplay/gameplay_optA.png')});
            assets.push({name: 'gameplay_optB', url: Utils.GetAssetUrl('images/gameplay/gameplay_optB.png')});
            assets.push({name: 'correctTick', url: Utils.GetAssetUrl('images/gameplay/correction_tick.png')});
            assets.push({name: 'tutHand', url: Utils.GetAssetUrl('images/gameplay/hand.png')});
            assets.push({name: 'arrowTut', url: Utils.GetAssetUrl('images/gameplay/arrowTut.png')});
            //
            assets.push({name: 'handYellowLeft', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_yellowback.png')});
            assets.push({name: 'handYellowRight', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_yellowfront.png')});
            assets.push({name: 'handBlueLeft', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_bluehandback.png')});
            assets.push({name: 'handBlueRight', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_bluehandfront.png')});
            assets.push({name: 'handPinkLeft', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_pinkhandback.png')});
            assets.push({name: 'handPinkRight', url: Utils.GetAssetUrl('images/gameplay/LevelEnd_Clapping/levelend_pinkhandfront.png')});
            //
            assets.push({name: 'endBackground', url: Utils.GetAssetUrl('images/level_end/background.png')});
            assets.push({name: 'endCta', url: Utils.GetAssetUrl('images/level_end/cta.png')});
            assets.push({name: 'endLogo', url: Utils.GetAssetUrl('images/level_end/logo.png')});
            assets.push({name: 'endSubtitle1', url: Utils.GetAssetUrl('images/level_end/sub_title_1.png')});
            assets.push({name: 'endTextBubble', url: Utils.GetAssetUrl('images/level_end/text_bubble.png')});
            assets.push({name: 'endTitle', url: Utils.GetAssetUrl('images/level_end/title.png')});
            assets.push({name: 'endMagnifier', url: Utils.GetAssetUrl('images/level_end/magnifier.png')});

            for (let i = 0; i< 5; i++) { assets.push({name: `confetti${i}`, url: Utils.GetAssetUrl(`images/gameplay/Confetti_Pieces/konfeti${i}.png`)}); }

            //sheet
            spritesheet.meta.image = Utils.GetAssetUrl("images/" + spritesheet.meta.image);
            assets.push({name: "spritesheet", url: "data:@file/json;base64," + Buffer.from(JSON.stringify(spritesheet)).toString("base64")});
            // for (let i = 0; i< GameDefine.PUZZLE_SIZE * GameDefine.PUZZLE_SIZE; i++){ assets.push({name: `puzzle${i}`, url: Utils.GetAssetUrl(`images/intro/Asset ${i+1}.png`)}); }
            //
            Utils.LoadFont(GameDefine.MAIN_FONT, Utils.GetAssetUrl("fonts/patrick-hand-regular.woff"));
            Utils.LoadAssets(assets, this.LoadCompleteHandler.bind(this), this.LoadProgressHandler.bind(this), this.LoadErrorhandler.bind(this));
        }
        else
        {
            this.mView.Init();
        }
    }

    Release()
    {
        // console.log('remove');
        // APP.stage.removeChild(this.mView);
        // this.mView.destroy({children:true, texture:true, baseTexture:true});
        // this.SetTouchable(false);
        super.Release();
    }

    Init()
    {
        // PIXI.loader.removeAllListeners();
        // PIXI.loader.reset();
    }

    LoadProgressHandler(loader, res)
    {

    }

    Update(dt)
    {
        // console.log(this.mView);
        this.mView.Update(dt);
    }

    LoadCompleteHandler(loader, res) 
    {
        // this.mView.SetResources(loader.resources);
        // this.mView.Init();
        // MainMusic.on('load', function(){
        //     // StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
        //     this.mView.SetResources(loader.resources);
        //     this.mView.Init();
        // }.bind(this));
        StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
        // StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
        // loader.removeAllListeners();
        // this.SetTouchable(true);
    }

    LoadErrorhandler(loader, res) 
    {
        
    }
}

module.exports = StatePreload;