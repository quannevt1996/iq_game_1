const BaseState = require('./BaseState');
const View = require('../views/StateIngameView');
const APP = require('../app');
// const MIDI = require('../libs/MIDI');
const Utils = require('../core/Utils');
const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');
const StateManager = require('./StateManager');
const GameDefine = require('../game/GameDefine');

class StateIngame extends BaseState {
    constructor() {
        super();
        //
        this.mView = new View();
        // this.addChild(this.mView);
        this.mState;
        let n = 0;
        this.STATE = {
            INIT: n++,
            RUN: n++,
            INACTIVE: n++,
        };
        this.mLoadingAssetLoaded = false;
    }

    Prelease() 
    {
        APP.stage.addChild(this.mView);
        // this.mView.scale.set(0.7);
        if (!this.mIsFirstInit) 
        {
            this.mIsFirstInit = true;
            //
            // console.log(PIXI.Loader.shared);
            let assets = [];
            // assets.push({name: "gameBG", url: Utils.GetAssetUrl("bg.jpg")});
            // assets.push({name: "test_ms", url: Utils.GetAssetUrl("sound_test.mid")});
            //
            Utils.LoadAssets(assets, this.LoadCompleteHandler.bind(this), this.LoadProgressHandler.bind(this), this.LoadErrorhandler.bind(this));

        }
        else
            this.mView.Init();
    }

    Release() {
        // this.SetTouchable(false);
        super.Release();
    }

    Init() {
        // PIXI.loader.removeAllListeners();
        // PIXI.loader.reset();
        this._SetState(this.STATE.INIT);
    }

    Start()
    {
        this._SetState(this.STATE.RUN);
    }

    Update(dt)
    {
        switch (this.mState)
        {
            case this.STATE.RUN:
                if (this.mView.alpha == 1)
                    this.mView.Update(dt);
            break;

            case this.STATE.INACTIVE:
            break;
        }
    }
    
    Pause()
    {
        this.mView.Pause();
    }

    Resume()
    {
        this.mView.Resume();
    }

    LoadProgressHandler(loader, res) 
    {
        
    }

    LoadCompleteHandler(loader, res) {
        // Utils.LoadFont('DancingScript' , (require('../fonts/DancingScript-Regular.ttf')).default);
        this.Init();
    }

    LoadErrorhandler(loader, res) {

    }

    //PRIVATE FUNCTION
    _SetState(s)
    {
        this.mState = s;
        switch (s) {
            case this.STATE.INIT:
                // this.mView.SetResources(loader.resources);
                this.mView.Init();
                this.Start();
                // this.mGameMgr.Init();
                //
                // this.mView.GameView = this.mGameMgr;
                EventManager.subscribe(EventDefine.STATE_INGAME_COMPLETE, function(){
                    // this.mState = this.STATE.INACTIVE;
                    let event = new CustomEvent("gameEnd", {  });
                    window.dispatchEvent(event);
                    // StateManager.SwitchState(GameDefine.GAME_STATE_END, true);
                }.bind(this));
                // StateManager.SwitchState(GameDefine.GAME_STATE_END, true);
            break;

            case this.STATE.RUN:
            break;
        }
    }
}

module.exports = StateIngame;