const BaseState = require('./BaseState');
const View = require('../views/StateLoadingView');
const APP = require('../app');
const StateManager = require('./StateManager');
const spritesheet = require('../assets/images/sprites.json');
const GameDefine = require('../game/GameDefine');
const Utils = require('../core/Utils');

class StateLoading extends BaseState
{
    constructor()
    {
        super();
        //
        this.mView = new View();
    }

    Prelease()
    {
        APP.stage.addChild(this.mView);
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            let assets = [];
            //images
            // assets.push({name: "bg", url: Utils.GetAssetUrl("images/bg.jpg")});
            // assets.push({name: "hand", url: Utils.GetAssetUrl("images/hand.png")});
            // assets.push({name: "greatCombo", url: Utils.GetAssetUrl("images/effect_wowtext_great.png")});
            // assets.push({name: "perfectCombo", url: Utils.GetAssetUrl("images/effect_wowtext_perfect.png")});
            // assets.push({name: "glassPiece", url: Utils.GetAssetUrl("images/effect_wowtext_pieces1.png")});
            // // assets.push({name: "hand_wheel", url: Utils.GetAssetUrl("images/hand_wheel.png")});
            // // assets.push({name: "installbutton", url: Utils.GetAssetUrl("images/installbutton.png")});
            // // assets.push({name: "logo", url: Utils.GetAssetUrl("images/logo.png")});
            // // assets.push({name: "nu_item_wheelInside", url: Utils.GetAssetUrl("images/nu_item_wheelInside.png")});
            // // assets.push({name: "WheelOutside", url: Utils.GetAssetUrl("images/WheelOutside.png")});
            // assets.push({name: "taptoplay", url: Utils.GetAssetUrl("images/taptoplay.png")});
            // // assets.push({name: "wheelTut", url: Utils.GetAssetUrl("images/wheelTut.png")});
            // // assets.push({name: "endbg", url: Utils.GetAssetUrl("images/endbg.jpg")});
            // assets.push({name: "cta", url: Utils.GetAssetUrl("images/cta.png")});
            // assets.push({name: "title", url: Utils.GetAssetUrl("images/titleend.png")});
            // assets.push({name: "giftbox", url: Utils.GetAssetUrl("images/giftbox.png")});
            // assets.push({name: "titleWin", url: Utils.GetAssetUrl("images/outtro/great_job.png")});
            // assets.push({name: "titleLose", url: Utils.GetAssetUrl("images/outtro/TRY AGAIN.png")});
            // assets.push({name: "ctaWin", url: Utils.GetAssetUrl("images/outtro/next level button.png")});
            // assets.push({name: "ctaLose", url: Utils.GetAssetUrl("images/outtro/replay-button.png")});
            // assets.push({name: "outtroHand", url: Utils.GetAssetUrl("images/outtro/hand.png")});
            // // for (let i = 0; i< 5; i++){ assets.push({name: `card${i+1}`, url: Utils.GetAssetUrl(`images/card${i+1}.png`)}); }
            // //sounds
            // // assets.push({name: "sound_mp3", url: Utils.GetAssetUrl("sounds/TheBlackEyedPeas_ads.mp3")});

            // //sheet
            // spritesheet.meta.image = Utils.GetAssetUrl("images/" + spritesheet.meta.image);
            // assets.push({name: "spritesheet", url: "data:@file/json;base64," + Buffer.from(JSON.stringify(spritesheet)).toString("base64")});
            //
            Utils.LoadAssets(assets, this.LoadCompleteHandler.bind(this), this.LoadProgressHandler.bind(this), this.LoadErrorhandler.bind(this));
        }
        else
        {
            this.mView.Init();
        }
    }

    Release()
    {
        // console.log('remove view');
        // APP.stage.removeChild(this.mView);
        // console.log(APP.stage.children);
        // this.SetTouchable(false);
        super.Release();
    }

    Init()
    {
        // PIXI.loader.removeAllListeners();
        // PIXI.loader.reset();
    }

    LoadProgressHandler(loader, res)
    {
        // console.log(loader.progress);
        this.mView.SetProgress(loader.progress);
        // this.mProgressText.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
    }

    LoadCompleteHandler(loader, res) 
    {
        // this.mView.SetResources(loader.resources);
        // this.mView.Init();
        // console.log('switch success');
        // console.log(json2);
        // console.log(loader.resources.spritesheet);

        StateManager.SwitchState(GameDefine.GAME_STATE_INGAME, true);
        // EventManager.publish(EventDefine.STATE_LOADING_COMPLETE, {});
    }

    LoadErrorhandler(loader, res) 
    {

    }
}

module.exports = StateLoading;