class EventDefine
{
    constructor()
    {
        let n = 0;
        this.ON_CONTAINER_RESIZE                = n++;
        this.STATE_LOADING_COMPLETE             = n++;
        this.ON_TOUCH                           = n++;
        this.ON_MOVE                            = n++;
        this.ON_RELEASE                         = n++;
        this.STATE_INGAME_COMPLETE              = n++;
        this.LEVEL_COMPLETED                    = n++;
        this.LEVEL_CONGRAT                      = n++;
    }
}

module.exports = new EventDefine();