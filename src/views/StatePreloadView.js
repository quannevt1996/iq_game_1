const BaseView = require('./BaseView');
const APP = require('../app');
const Input = require('../core/Input');

class StatePreloadView extends BaseView
{
    constructor()
    {
        super();
        //
        this.mBackground = new PIXI.Container();
        this.mForeground = new PIXI.Container();
        //
        this.addChild(this.mBackground);
        this.addChild(this.mForeground);
        //
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            // let bg = new PIXI.Sprite(Resources.introBg.texture);
            // bg.anchor.set(0.5);
            // bg.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
            //
            this.Scaling();
            this.Positioning();
            this.EventHandler();
            this.SetTouchable(true);
        }
    }

    Scaling()
    {
        if (APP.GetWidth() < APP.GetHeight())
        {

        }
        else
        {

        }
    }

    Positioning()
    {
        if (APP.GetWidth() < APP.GetHeight())
        {
            
        }
        else
        {

        }
    }

    EventHandler()
    {

    }

    Update(dt)
    {
        // this.mCardView.Update(dt);
    }

    TouchHandler(event)
    {
        // console.log(event.type);
        // this.mCardView.TouchHandler(event);
        // switch (event.target) {
        // }
        // console.log(event);
        if (Input.IsTouchDown(event))
        {
            // if (this.onCardPick && this.mDonePickView.alpha == 1)
            // {
            //     StateManager.SwitchState(GameDefine.GAME_STATE_LOADING, true);
            //     // StateManager.SwitchState(GameDefine.GAME_STATE_END, true);
            // }
        }
    }
}

module.exports = StatePreloadView;