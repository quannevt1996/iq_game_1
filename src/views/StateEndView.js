const BaseView = require('./BaseView');
const APP = require('../app');

class StateEndView extends BaseView {
    constructor() {
        super();
        //
        this.mBackgroundLayer = new PIXI.Container();
        this.mForegroundLayer = new PIXI.Container();
        //
        this.addChild(this.mBackgroundLayer);
        this.addChild(this.mForegroundLayer);
        //
        this.mBg = null;
        this.mIsFirstInit = false;
    }

    Init() 
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            // let s = new PIXI.Sprite(Resources.introBg.texture)
            // s.scale.set(APP.GetWidth()/s.width);
            //
            // this.mForegroundLayer.addChild(this.mBg);
            //
            this.Scaling();
            this.Positioning();
            this.SetTouchable(true);
            this.EventHandler();
        }
    }

    Scaling()
    {
        if (APP.GetWidth() < APP.GetHeight())
        {

        }
        else
        {

        }
    }

    Positioning() 
    {
        if (APP.GetWidth() < APP.GetHeight())
        {

        }
        else
        {
            
        }
    }

    Update(dt)
    {
        
    }

    EventHandler()
    {

    }

    TouchHandler(event) 
    {
        if (Input.IsTouchDown(event))
        {
            // console.log(event.currentTarget, this.mCardView);
            if (event.target == this.mCTA || event.currentTarget instanceof CardView)
            {
                let event = new CustomEvent('onCTAClick', {
                    detail: {url: 'https://apps.apple.com/us/app/magic-tiles-3-piano-game/id1443446174'}
                });
                window.dispatchEvent(event);
            }
        }
        if (Input.IsTouchUp(event))
        {

        }
    }
}

module.exports = StateEndView;