const GameDefine = require('../../game/GameDefine');
const APP = require('../../app');
const Input = require('../../core/Input');
const Utils = require('../../core/Utils');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const Filters = require('@pixi/filter-bulge-pinch');

class EndView extends PIXI.Container
{
    constructor()
    {
        super();
        this.mIsFirstInit = false;
        this.mScoreText = {};
        this.background = null;
        this.glass = null;
        this.title = null;
        this.subTitle = null;
        this.logo = null;
        this.cta = null;
        this.bubble = null;
        this.magnify = null;
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: 'white',
                fontSize: '60px',
                lineJoin: "bevel",
                stroke: "black",
                strokeThickness: 1,
                fontWeight: 'bold'
            });
            //
            this.mIsFirstInit = true;
            // this.background = new PIXI.Graphics();
            // this.background.beginFill(0xd3eef5, 0.5);
            // this.background.drawRect(0, 0, APP.GetWidth(), APP.GetHeight());
            this.background = new PIXI.Sprite(Resources.endBackground.texture);
            // this.background.scale.set(1);
            // this.background.scale.set(APP.GetWidth()/this.background.width ,APP.GetHeight()/this.background.height);
            //
            this.logo = new PIXI.Sprite(Resources.endLogo.texture);
            this.logo.anchor.set(0.5);
            this.bubble = new PIXI.Sprite(Resources.endTextBubble.texture);
            this.bubble.anchor.set(0.5);
            this.title = new PIXI.Sprite(Resources.endTitle.texture);
            this.title.anchor.set(0.5);
            this.subTitle = new PIXI.Sprite(Resources.endSubtitle1.texture);
            this.subTitle.anchor.set(0.5);
            this.cta = new PIXI.Sprite(Resources.endCta.texture);
            this.cta.anchor.set(0.5);
            this.cta.baseScale = 1;
            this.cta.deltaTime = 0;
            this.cta.dir = 1;
            this.glass = new PIXI.Sprite(Resources.endMagnifier.texture);
            this.glass.anchor.set(0.36, 0.3);
            this.glass.dir = 1;
            this.magnify = new Filters.BulgePinchFilter({});
            this.magnify.center = [-1000, -1000];
            this.filters = [this.magnify];
            //
            this.addChild(this.background);
            this.addChild(this.logo);
            this.addChild(this.bubble);
            this.addChild(this.title);
            this.addChild(this.subTitle);
            this.addChild(this.cta);
            this.addChild(this.glass);
            //
            this.Scaling();
            this.Positioning();
            this.EventHandler();
        }
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.ON_TOUCH, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_MOVE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_RELEASE, function(data){
            // console.log(data);
            this.TouchHandler(data.event);
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_CONTAINER_RESIZE, function(e){
            // this.background.scale.set(e.data.width/this.background.width);
            // this.background.scale.set(1);
            // this.background.scale.set(e.data.width/this.background.width ,e.data.height/this.background.height);
            this.Scaling();
            this.Positioning();
        }.bind(this));
    }

    Scaling()
    {
        this.background.scale.set(1);
        this.background.scale.set(APP.GetWidth()/this.background.width ,APP.GetHeight()/this.background.height);
        if (APP.GetWidth() > APP.GetHeight())
        {
            // this.background.scale.set(APP.GetWidth()/this.background.width);
            this.logo.scale.set(0.55);
            this.cta.scale.set(0.45);
            this.title.scale.set(0.45);
            this.subTitle.scale.set(0.65);
            this.bubble.scale.set(0.4);
            this.glass.scale.set(0.55);
            this.magnify.radius = 200*this.glass.scale.x;
        }
        else
        {
            // this.background.scale.set(APP.GetWidth()/this.background.width ,APP.GetHeight()/this.background.height);
            this.logo.scale.set(0.65/GameDefine.SCALE_CANVAS);
            this.cta.scale.set(0.85/GameDefine.SCALE_CANVAS);
            this.title.scale.set(0.85/GameDefine.SCALE_CANVAS);
            this.subTitle.scale.set(1/GameDefine.SCALE_CANVAS);
            this.bubble.scale.set(0.9/GameDefine.SCALE_CANVAS);
            this.glass.scale.set(0.75/GameDefine.SCALE_CANVAS);
            this.magnify.radius = 200*this.glass.scale.x;
        }
        this.cta.baseScale = this.cta.scale.x;
    }

    Positioning()
    {
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.logo.position.set(0.2*APP.GetWidth(), 0.5*APP.GetHeight());
            this.bubble.position.set(0.75*APP.GetWidth(), 0.5*APP.GetHeight() - 100);
            this.title.position.set(0.75*APP.GetWidth(), 0.5*APP.GetHeight() - 200);
            this.subTitle.position.set(this.title.position.x, this.title.position.y + 0.5* this.title.height + 100);
            this.cta.position.set(0.75*APP.GetWidth(), APP.GetHeight() - 100);
            this.glass.position.set(0 - 600, this.title.position.y);
        }
        else
        {
            this.logo.position.set(0.5*APP.GetWidth(), 250);
            this.bubble.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
            this.title.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() - 200/GameDefine.SCALE_CANVAS);
            this.subTitle.position.set(this.title.position.x, this.title.position.y + 0.5* this.title.height + 200/GameDefine.SCALE_CANVAS);
            this.cta.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight() + 500/GameDefine.SCALE_CANVAS);
            this.glass.position.set(0 - 600, this.title.position.y);
        }
    }

    Update(dt)
    {
        this.magnify.center = [this.glass.position.x/APP.GetWidth(), this.glass.position.y/APP.GetHeight()];
        this.cta.scale.set(this.cta.baseScale/GameDefine.SCALE_CANVAS);
        this.glass.position.x = Math.max(-1000, Math.min(APP.GetWidth() + 1000, this.glass.position.x + 700*this.glass.dir*dt));
        if (this.glass.position.x == -1000 || this.glass.position.x == APP.GetWidth() + 1000)
            this.glass.dir *= -1;
        if (APP.GetWidth() > APP.GetHeight())
        {
            this.cta.baseScale = this.easeInOutBack(this.cta.deltaTime, 0.55, 0.15, 4);
            this.cta.deltaTime = Math.max(0, Math.min(4, this.cta.deltaTime + 3*this.cta.dir*dt));
            if (this.cta.deltaTime == 0 || this.cta.deltaTime == 4)
                this.cta.dir *= -1;
        }
        else
        {
            //
            this.cta.baseScale = this.easeInOutBack(this.cta.deltaTime, 0.85, 0.25, 4);
            this.cta.deltaTime = Math.max(0, Math.min(4, this.cta.deltaTime + 3*this.cta.dir*dt));
            if (this.cta.deltaTime == 0 || this.cta.deltaTime == 4)
                this.cta.dir *= -1;
        }
    }

    /**
     * 
     * @param {*} t current time
     * @param {*} b start value
     * @param {*} c change value
     * @param {*} d duration
     */
    easeInOutBack(t, b, c, d)
    {
        let s = 1.70158;
        if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525)) + 1) * t - s)) + b;
        return c / 2 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2) + b;
    }

    TouchHandler(e)
    {
        if (Input.IsTouchDown(e))
        {
            if (Utils.PointInRect({x: e.data.global.x, y: e.data.global.y}, this.cta.getBounds()))
            {
                let event = new CustomEvent('onCTAClick', {
                    detail: {url: GameDefine.REDIRECT_URL}
                });
                window.dispatchEvent(event);
            }
        }
    }
}

module.exports = EndView;