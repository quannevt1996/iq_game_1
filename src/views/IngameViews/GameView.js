const GameMgr = require('../../game/GameMgr');
const GameDefine = require('../../game/GameDefine');
const HandClap = require('../../game/HandClap');
const APP = require('../../app');
const Timer = require('../../core/Timer');
const EventManager = require('../../events/EventManager');
const EventDefine = require('../../events/EventDefine');
const { Howl } = require('howler');

class GameView extends PIXI.Container
{
    constructor()
    {
        super();
        this.mGameMgr;
        this.mIsFirstInit = false;
        this.mScoreText = {};
        this.handClaps = new PIXI.Container();
        this.congratTime = new Timer();
        this.isCongrat = false;
        this.applauseSound = new Howl({
            src: Utils.GetAssetUrl("sounds/applause.mp3"),
            autoplay: false,
            loop: false
        });
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            const style = new PIXI.TextStyle({
                fontFamily: GameDefine.MAIN_FONT.name,
                fill: 'white',
                fontSize: '60px',
                lineJoin: "bevel",
                stroke: "black",
                strokeThickness: 1,
                fontWeight: 'bold'
            });
            //
            this.mIsFirstInit = true;
            this.mGameMgr = new GameMgr();
            //
            this.InitHandClap();
            this.handClaps.alpha = 0;
            this.mGameMgr.Init();
            this.congratTime.SetDuration(2);
            //
            this.addChild(this.mGameMgr);
            this.addChild(this.handClaps);
            this.Positioning();
            this.EventHandler();
            
            // this.mainGameMusic = new Howl({
            //     src: [Utils.GetAssetUrl("sounds/MainMusic.mp3")],
            //     // autoplay: true,
            //     loop: true,
            //     volume: 0.75,
            //     html5: true
            // });
            this.PlayMainSound();
        }
    }

    InitHandClap()
    {
        let a = new HandClap({
            leftHand: Resources.handYellowLeft.texture,
            rightHand: Resources.handYellowRight.texture
        });
        let b = new HandClap({
            leftHand: Resources.handBlueLeft.texture,
            rightHand: Resources.handBlueRight.texture
        });
        let c = new HandClap({
            leftHand: Resources.handPinkLeft.texture,
            rightHand: Resources.handPinkRight.texture
        });
        let d = new HandClap({
            leftHand: Resources.handYellowLeft.texture,
            rightHand: Resources.handYellowRight.texture
        });
        this.handClaps.addChild(a, b, c, d);
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.LEVEL_CONGRAT, function(){
            this.isCongrat = true;
            this.congratTime.Reset();
            this.applauseSound.play();
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_CONTAINER_RESIZE, function(){
            this.Positioning();
        }.bind(this));
    }

    Positioning()
    {
        this.handClaps.children[0].position.set(0.75*APP.GetWidth(), APP.GetHeight() + 150);
        this.handClaps.children[1].position.set(-0.1*APP.GetWidth(), APP.GetHeight());
        this.handClaps.children[1].rotation = Math.PI/6;
        this.handClaps.children[2].position.set(0.35*APP.GetWidth(), APP.GetHeight() + 150);
        this.handClaps.children[3].position.set(1.15*APP.GetWidth(), APP.GetHeight() - 150);
        this.handClaps.children[3].rotation = -Math.PI/4;
        
    }

    Update(dt)
    {
        if (this.mGameMgr)
            this.mGameMgr.Update(dt);
        if (this.isCongrat)
        {
            this.handClaps.alpha = Math.min(1, this.handClaps.alpha + 2*dt);
            this.congratTime.Update(dt);
            if (this.congratTime.IsDone())
                this.isCongrat = false;
        }
        else
        {
            this.handClaps.alpha = Math.max(0, this.handClaps.alpha - 2*dt);
        }
        if (this.handClaps.children.length > 0)
            this.handClaps.children.forEach((c) => { c.Update(dt) });
    }

    Pause()
    {
        this.sound.pause();
    }

    Resume()
    {
        this.sound.play();
    }

    PlayMainSound()
    {
        // var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
        // if (!isChrome){
        //     document.getElementById('iframeAudio').remove();
        // }
        // else {
        //     document.getElementById('playAudio').remove();// just to make sure that it will not have 2x audio in the background 
        // }
        // console.log(Utils.GetAssetUrl("sounds/MainMusic.mp3"));
        this.sound = new Howl({
            src: [Utils.GetAssetUrl("sounds/MainMusic.mp3")],
            html5: false,
            loop: true,
            preload: true,
            autoplay: true,
        });
        // this.sound.once('load', () => {
        //     // this.idSoundPlaying = this.sound.play();
        // })
        // this.sound.once('loaderror', () => {
        //     Howler.unload();
        // });
    
        // this.sound.once('playerror', () => {
        //     this.sound.unload(false);
        //     this.sound._duration = 0; // init duration
        //     // this.sound._sprite = {};// init sprite
        //     this.sound._src = Utils.GetAssetUrl("sounds/MainMusic.mp3");
        //     this.sound.load();
        // });
        // this.sound.once('stop', () => {
        // });
    
        // this.sound.once('unlock', () => {
        // });
    
        // this.sound.once("end", () => {
        //     this.sound.stop(this.idSoundPlaying);
        // })
        //
        // var url = Utils.GetAssetUrl("sounds/MainMusic.mp3");
        // window.AudioContext = window.AudioContext||window.webkitAudioContext; //fix up prefixing
        // var context = new AudioContext(); //context
        // var source = context.createBufferSource(); //source node
        // source.connect(context.destination); //connect source to speakers so we can hear it
        // var request = new XMLHttpRequest();
        // request.open('GET', url, true); 
        // request.responseType = 'arraybuffer'; //the  response is an array of bits
        // request.onload = function() {
        //     context.decodeAudioData(request.response, function(response) {
        //         source.buffer = response;
        //         source.start(0); //play audio immediately
        //         source.loop = true;
        //     }, function () { console.error('The request failed.'); } );
        // }
        // request.send();
    }
}

module.exports = GameView;