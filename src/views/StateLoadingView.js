const BaseView = require('./BaseView');
const APP       = require('../app');
const GameDefine = require('../game/GameDefine');

class StateLoadingView extends BaseView
{
    constructor()
    {
        super();
        //
        this.mBackground = new PIXI.Container();
        this.mForeground = new PIXI.Container();
        //
        const style = new PIXI.TextStyle({
            fontFamily: GameDefine.MAIN_FONT.name,
            fill: 'white'
        });
        this.mProgressText = new PIXI.Text('', style);
        this.mProgressText.anchor.set(0.5);
        this.mProgressText.position.set(0.5*APP.GetWidth(), 0.5*APP.GetHeight());
        //
        this.addChild(this.mProgressText);
        this.addChild(this.mBackground);
        this.addChild(this.mForeground);
    }

    Init()
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            this.removeChild(this.mProgressText);
            this.mProgressText = null;
            //
            this.Positioning();
        }
    }

    SetProgress(progress)
    {
        this.mProgressText.text = progress + ' %';
    }

    Positioning()
    {

    }

    TouchHandler(event)
    {
        // console.log(event);
        switch (event.target) {
        }
    }
}

module.exports = StateLoadingView;