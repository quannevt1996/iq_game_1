const BaseView = require('./BaseView');
const APP = require('../app');
const GameView = require('./IngameViews/GameView');
const EventManager = require('../events/EventManager');
const EventDefine = require('../events/EventDefine');
const GameDefine = require('../game/GameDefine');
const MainMusic = require('../game/MainMusic');
const Input = require('../core/Input');
const Timer = require('../core/Timer');
const EndView = require('./IngameViews/EndView');
const { Howl } = require('howler');
// const EndView = require('./IngameViews/EndView');

class StateIngameView extends BaseView {
    constructor() {
        super();
        //
        this.mBackgroundLayer = new PIXI.Container();
        this.mForegroundLayer = new PIXI.Container();
        //
        this.mGameView = new GameView();
        this.mEndView = new EndView();
        //
        this.addChild(this.mBackgroundLayer);
        this.addChild(this.mForegroundLayer);
        //
        this.mBg = null;
        // this.mMusic = new MainMusic({
        //     pauseDuration: GameDefine.GAME_TIME_PAUSE_BEFORE_PLAY,
        //     src: [Utils.GetAssetUrl("sounds/DanceMonkeyCover_TonesAndI.mp3")]
        //     // src: [Utils.GetAssetUrl("sounds/TheBlackEyedPeas_ads.mp3")]
        // });
        this.mMusic = MainMusic;
        this.mPlayTimer = new Timer();
        this.isGameComplete = false;
        this.endViewSwish = new Howl({
            src: [Utils.GetAssetUrl("sounds/swish.mp3")],
            autoplay: false,
            loop: false
        });
        this.mIsEndViewTransitionCompleted = false;
        // this.mEndView = null;
    }

    Init() 
    {
        if (!this.mIsFirstInit)
        {
            this.mIsFirstInit = true;
            //
            this.mGameView.Init();
            this.mEndView.Init();
            // this.mBackground.anchor.set(0.5);
            // if (APP.GetWidth() > APP.GetHeight())
            //     s.scale.set(APP.GetWidth()/s.width);
            // else
            //     s.scale.set(APP.GetWidth()/s.width ,APP.GetHeight()/s.height);
            //
            // let f = new PIXI.filters.BlurFilter();
            // this.filters = [f];
            this.addChild(this.mGameView);
            this.addChild(this.mEndView);
            //
            this.Scaling();
            this.Positioning();
            this.SetTouchable(true);
            this.EventHandler();
            // this.mEndView.Hide();
        }
    }

    Positioning() 
    {
        this.mEndView.position.set(0, APP.GetHeight());
        // this.mEndView.position.set(0, 0);
    }

    Scaling()
    {

    }

    Update(dt)
    {
        this.mGameView.Update(dt);
        // this.mEndView.Update(dt);
        this.mMusic.Update(dt);
        this.mPlayTimer.Update(dt);
        if (this.isGameComplete)
        {
            this.mEndView.Update(dt);
            this.mEndView.position.set(
                0,
                this.mEndView.position.y - 5*(this.mEndView.position.y - 0)*dt
            );
            if (this.mEndView.position.y <= 10)
                this.mIsEndViewTransitionCompleted = true;
            // console.log(this.mEndView.position);
        }
        // console.log(this.mPlayTimer.GetTime());
        // this.mEndView.Update(dt);
        // if (this.mPlayTimer.IsDone())
        // {
        //     // console.log('done');
        //     EventManager.publish(EventDefine.STATE_INGAME_COMPLETE, {condition: GameDefine.IS_GAME_WIN});
        // }
    }

    EventHandler()
    {
        EventManager.subscribe(EventDefine.STATE_INGAME_COMPLETE, function(){
            this.isGameComplete = true;
            let f = new PIXI.filters.BlurFilter();
            this.mGameView.filters = [f];
            this.endViewSwish.play();
        }.bind(this));
        EventManager.subscribe(EventDefine.ON_CONTAINER_RESIZE, function(){
            if (!this.mIsEndViewTransitionCompleted)
                this.mEndView.position.set(0, APP.GetHeight());
            else
                this.mEndView.position.set(0, 0);
        }.bind(this));
    }

    Pause()
    {
        // this.mMusic.pause();
        this.mGameView.Pause();
    }

    Resume()
    {
        // this.mMusic.play();
        this.mGameView.Resume();
    }

    TouchHandler(event) 
    {
        if (Input.IsTouchDown(event))
        {
            EventManager.publish(EventDefine.ON_TOUCH, {event: event});
        }
        if (Input.IsTouchUp(event))
        {
            EventManager.publish(EventDefine.ON_RELEASE, {event: event});
        }
        if (Input.IsTouchMove(event))
        {
            EventManager.publish(EventDefine.ON_MOVE, {event: event});
        }
    }
}

module.exports = StateIngameView;