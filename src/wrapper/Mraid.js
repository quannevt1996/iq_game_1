const Index = new (require('../index').default);

function doReadyCheck()
{
    if (!mraid)
    {
        mraid = window.mraid;
        console.warn('MRAID is not available');
    }
    // var success = false;
    // if (document.readyState === 'complete') {
    //     if (typeof mraid !== 'undefined') {
    //         if (mraid.getState() === 'loading') {
    //             mraid.addEventListener('ready', showMyAd);
    //         } else if (mraid.getState() === 'default') {
    //             showMyAd();
    //         }
    //         success = true;
    //     }
    // }
    // return success;
    if (mraid.getState() == 'loading')
    {
        //Wait until mraid library is ready and loaded so listen for ready event
        mraid.addEventListener("ready", mraidIsReady);
        console.log('Added on ready listener');
    }
    else
    {
        showMyAd();
    }
}

function showMyAd()
{
    Index.OnReady();
    if (mraid.isViewable()) {
        adVisibleCallback({ isViewable: true });
    }

    mraid.addEventListener('viewableChange', viewable => {
        adVisibleCallback({ isViewable: viewable });
    });

    mraid.addEventListener("stateChange", () => {
        console.log("--- State change event raised: ", mraid.getState());
        if (mraid.getState() == 'hidden') {
            window.dispatchEvent(new CustomEvent('gamePause', {}));
        }
    });

    window.removeEventListener('resize', resizeAds, false);
    window.removeEventListener('onCTAClick', clickWebCTA);
    window.removeEventListener("visibilitychange", viewChangeHandler);
    window.addEventListener('onCTAClick', onCTAClick);
    mraid.addEventListener("sizeChange", adResizeCallback);
    mraid.addEventListener("error", (e) => { console.error(e) });
    mraid.addEventListener("info", (e) => { console.log(e) });
}

function mraidIsReady()
{
    //Remove the ready event listener
    mraid.removeEventListener("ready", mraidIsReady);
    showMyAd();
}

function adVisibleCallback(event) {
    let e;
    if (event.isViewable) {
        e = new CustomEvent('gameResume', {});
    } else {
        e = new CustomEvent('gamePause', {});
    }
    window.dispatchEvent(e);
}

function adResizeCallback() {
    let screenSize = mraid.getScreenSize();
    // this.emit("resize", screenSize);
    Index.Resize(screenSize);
}

// function audioVolumeChangeCallback() {
//     // HERE
//     let isAudioEnabled = !!volume;
//     let e = new CustomEvent('audioChange', {volume: isAudioEnabled});
//     window.dispatchEvent(e);
//     // game.setMute(!isAudioEnabled);
// }

function onCTAClick(url) {
    url = url.detail.url;
    mraid.open(url);
    try {
        FbPlayableAd.onCTAClick();
    } catch (error) {
        
    }
}

function resizeAds(){
    const el = document.getElementById('canvasContainer');
    Index.Resize({width: el.clientWidth, height: el.clientHeight});
}

function clickWebCTA(e)
{
    window.open(e.detail.url, '_blank');
}

doReadyCheck();
// setInterval(function(){
//     console.log(mraid.getState());
// }, 1000);
// console.log(mraid, window.mraid);
// (function(){
//     Index.Resize();
// }())
Index.OnReady();
resizeAds();
// setTimeout(function(){
//     Index.Resize();
// }, 3000);
// resizeAds();
// Index.Resize({ width: innerWidth, height: innerHeight });
function viewChangeHandler()
{
    let ev;
    if (!document.hidden)
    {
        ev = new CustomEvent('gameResume', {});
    }
    else
        ev = new CustomEvent('gamePause', {});
    window.dispatchEvent(ev);
}

window.addEventListener("visibilitychange", viewChangeHandler);
window.addEventListener('resize', resizeAds, false);
window.addEventListener('onCTAClick', clickWebCTA);