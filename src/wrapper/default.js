const Index = new (require('../index').default);

Index.OnReady();
Index.Resize({ width: innerWidth, height: innerHeight });

window.addEventListener('resize', (event) => {
    const el = document.getElementById('canvasContainer');
    Index.Resize({width: el.clientWidth, height: el.clientHeight});
}, false);

window.addEventListener('onCTAClick', (e) => {
    window.open(e.detail.url, '_blank');
});

window.addEventListener("visibilitychange", (e) => {
    // document.title = document.hidden ? "I'm away" : "I'm here";
    let ev;
    if (!document.hidden)
    {
        ev = new CustomEvent('gameResume', {});
    }
    else
        ev = new CustomEvent('gamePause', {});
    window.dispatchEvent(ev);
});