const Index = new (require('../index').default);
// const Index = require('../index');
var onloaded = false;

window.onload = function(){
	if (!onloaded)
	{
		(dapi.isReady()) ? onReadyCallback() : dapi.addEventListener("ready", onReadyCallback);
		onloaded = true;
	}
	//here you can put other code that not related to dapi logic
};

window.addEventListener('DOMContentLoaded', function(){
	if (!onloaded)
	{
		if (dapi.isReady())
			onReadyCallback()
		else
		{
			dapi.addEventListener("ready", function(){
				onReadyCallback();
			});
		}
		onloaded = true;
	}
		// (dapi.isReady()) ? onReadyCallback() : dapi.addEventListener("ready", onReadyCallback);
	// console.log(Index);
	// Index.OnReady();
});

function onReadyCallback(){
	//no need to listen to this event anymore
	dapi.removeEventListener("ready", onReadyCallback);
	let isAudioEnabled = !!dapi.getAudioVolume();
	Index.OnReady();

	if(dapi.isViewable()){
		adVisibleCallback({isViewable: true});
	}

	dapi.addEventListener("viewableChange", adVisibleCallback);
	dapi.addEventListener("adResized", adResizeCallback);
    dapi.addEventListener("audioVolumeChange", audioVolumeChangeCallback);
    window.addEventListener('onCTAClick', openCTALink);
}

function adVisibleCallback(event){
    console.log("isViewable " + event.isViewable);
    let e;
	if (event.isViewable){
        screenSize = dapi.getScreenSize();
        Index.Resize(screenSize);
        //START or RESUME the ad
        e = new CustomEvent('gameResume', {});
	} else {
        //PAUSE the ad and MUTE sounds
        e = new CustomEvent('gamePause', {});
    }
    window.dispatchEvent(e);
}

function adResizeCallback(event){
    screenSize = event;
    Index.Resize(event);
	console.log("ad was resized width " + event.width + " height " + event.height);
}

//When user clicks on the download button - use openStoreUrl function
function openCTALink(event){
	dapi.openStoreUrl();
}

function audioVolumeChangeCallback(volume){
	let isAudioEnabled = !!volume;
	if (isAudioEnabled){
		//START or turn on the sound
	} else {
		//PAUSE the turn off the sound
	}
}