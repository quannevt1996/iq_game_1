const Index = new (require('../index').default);

window.gameStart = function(){  };
window.gameClose = function(){  };

window.addEventListener("load", function(event) {
    console.log("All DOM loaded");
    window.gameReady && window.gameReady();
    Index.OnReady();
    const el = document.getElementById('canvasContainer');
    Index.Resize({width: el.clientWidth, height: el.clientHeight});
});

window.addEventListener("gameEnd", function(e) { 
    window.gameEnd && window.gameEnd();
});

window.addEventListener('resize', (event) => {
    const el = document.getElementById('canvasContainer');
    Index.Resize({width: el.clientWidth, height: el.clientHeight});
}, false);

window.addEventListener('onCTAClick', () => {
    // window.open(url, '_blank');
    window.install && window.install();
});